Function{ 
    myDomains = {}; // domains this process is responsible for
    // list of fields, used to save solution to later
    ListOfFields = {};
    ListOfConnectedFields = {};
    
    For dom_idx In {0:no_of_subdomains-1}
        myDomains~{dom_idx} = {};
        If (dom_idx % MPI_Size == MPI_Rank)
            myDomains() += Domains(dom_idx);
            myDomains~{dom_idx} += Domains~{dom_idx}();
        EndIf
    EndFor
    
    // g_in
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        //debug
        tmp_str = Sprintf["Treating subdomain i=%g. Neighbors are: ", i];
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            tmp_str = StrCat[tmp_str, Sprintf["j=%g ", j]];
        EndFor
        Printf[StrCat[tmp_str]];
        //debug
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            
            tag_g_rz~{i}~{j} = 1000 * i + j;
            tag_g_rz~{j}~{i} = 1000 * j + i;
            
            tag_g_phi~{i}~{j} = 1e+06 + 1000 * i + j;
            tag_g_phi~{j}~{i} = 1e+06 + 1000 * j + i;
            
            Printf[ Sprintf["Adding tags for i=%g, j=%g to ListOfFields", i, j]];
            ListOfFields() += tag_g_rz~{i}~{j}; 
            ListOfFields() += tag_g_phi~{i}~{j};
            Printf[ Sprintf["Adding tags for j=%g, i=%g to ListOfConnectedFields", j, i]];
            ListOfConnectedFields () += 2;
            ListOfConnectedFields () += tag_g_rz~{j}~{i};
            ListOfConnectedFields () += tag_g_phi~{j}~{i};
                    
            g_in_rz~{i}~{j}[Sigma~{i}~{j}] = ComplexVectorField[XYZ[]]{tag_g_rz~{j}~{i}};
            If (ansatz == 1)
                g_in_phi~{i}~{j}[Sigma~{i}~{j}] = ComplexScalarField[XYZ[]]{tag_g_phi~{j}~{i}};
            ElseIf (ansatz == 2)
                g_in_phi_star~{i}~{j}[Sigma~{i}~{j}] = ComplexScalarField[XYZ[]]{tag_g_phi~{j}~{i}};
            EndIf
        EndFor
    EndFor
}
