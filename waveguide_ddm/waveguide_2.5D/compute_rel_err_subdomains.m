function compute_rel_err_subdomains(m_max=2, n_max=3, start_sub=2, sub_steps=4, print_figures=false, ...
  source_file='convergence_waveguide/subdomains/high_order/convergence_study_raw', target_file='convergence_waveguide/subdomains/high_order/convergence_waveguide_results', figure_dest='../../../figures/convergence/ddm/Q3D/subdomains/high_order/')
data=load(source_file)(:, 2); # load source file. Only take 2nd column (real part of L2-Norm)
idx=1; # starting index
#rel_errs = zeros(2, m_max+1, n_max, subdomain_count, 2, res_steps+1); # save rel erros here. 
errs_subdomain = zeros(2, m_max+1, n_max, sub_steps, 2);
norms_subdomain = zeros(2, m_max+1, n_max, sub_steps, 2);
# Index scheme is {1 for TE, 2 for TM}, {m+1}, {n}, {subdomain index}, {1 for inplane, 2 for outofplane}, {refinement step}

for type=1:2
    for m=0:m_max
        for n=1:n_max
            for s=0:sub_steps
		            sub = start_sub * 2 ^ s;
		            idx_step = 4;
                # extract squared err norm, squared analytical solution for in- and outofplane 
                squ_err_inplane = sum(data([idx:idx_step:idx+idx_step*(sub-1)]));
                squ_norm_inplane = sum(data([idx+1:idx_step:idx+idx_step*(sub-1)+1]));
                squ_err_outofplane = sum(data([idx+2:idx_step:idx+idx_step*(sub-1)+2]));
                squ_norm_outofplane = sum(data([idx+3:idx_step:idx+idx_step*(sub-1)+3]));
                #
                errs_subdomain(type, m+1, n, s+1, 1) = squ_err_inplane;
                norms_subdomain(type, m+1, n, s+1, 1) = squ_norm_inplane;
                errs_subdomain(type, m+1, n, s+1, 2) = squ_err_outofplane;
                norms_subdomain(type, m+1, n, s+1, 2) = squ_norm_outofplane;
                idx += idx_step * sub;
            end
            #idx+=idx_step*sub; # step to beginning of next mode
        end
    end
end

iterations = zeros(2, m_max+1, n_max, sub_steps+1);
it_data = load('convergence_waveguide/subdomains/high_order/convergence_study_iterations_raw');
idx=1;
for type=1:2
    for m=0:m_max
        for n=1:n_max
                idx_step=1;
                iterations(type, m+1, n, :) = squeeze(it_data([idx:idx_step:idx+idx_step*(sub_steps)]));
                idx += idx_step * (sub_steps+1);
        end
    end
end

xvalues = start_sub .* 2.^[0:sub_steps];
### slope
convergence_slope = @(xval, yval) ...
(log2(yval(2:end)) - log2(yval(1:end-1))) ./ (log2(xval(2:end)) - log2(xval(1:end-1)));

# 1) compute relative error for solution of whole domain.
rel_err_whole_domain = sqrt(errs_subdomain) ./ sqrt(norms_subdomain);

for mode_type =1:2
    for plane=1:2
      tmp_mat = zeros(sub_steps+1, m_max+1 + n_max);
      tmp_str = "subdomains";
      for m=0:m_max
        for n=1:n_max
          tmp_mat(:, m*n_max+n) = rel_err_whole_domain(mode_type, m+1, n, :, plane);
          tmp_str = strcat(tmp_str, sprintf(",m=%g-n=%g",m,n));
        end
      end
      tmp_file_str = strcat(figure_dest, "/raw/waveguide");
      if (mode_type == 1) tmp_file_str = strcat(tmp_file_str, "_te"); else tmp_file_str = strcat(tmp_file_str, "_tm"); end
      if (plane == 1) tmp_file_str = strcat(tmp_file_str, "_inplane"); else tmp_file_str = strcat(tmp_file_str, "_outofplane"); end
      tmp_file_str = strcat(tmp_file_str, ".csv");
      tmp_file = fopen(tmp_file_str, 'w');
      fputs(tmp_file, strcat(tmp_str, "\n"));

      csvwrite( tmp_file, ...
        [xvalues', tmp_mat]);
      fclose(tmp_file);
    end
end
clear tmp_file_str; 
clear tmp_file;
clear tmp_mat;
clear tmp_str;
%
%
for mode_type =1:2
      tmp_mat = zeros(sub_steps+1, m_max+1 + n_max);
      tmp_str = "subdomains";
      for m=0:m_max
        for n=1:n_max
          tmp_mat(:, m*n_max+n) = iterations(mode_type, m+1, n, :);
          tmp_str = strcat(tmp_str, sprintf(",m=%g-n=%g",m,n));
        end
      end
      tmp_file_str = strcat(figure_dest, "/raw/iterations_waveguide");
      if (mode_type == 1) tmp_file_str = strcat(tmp_file_str, "_te"); else tmp_file_str = strcat(tmp_file_str, "_tm"); end
      if (plane == 1) tmp_file_str = strcat(tmp_file_str, "_inplane"); else tmp_file_str = strcat(tmp_file_str, "_outofplane"); end
      tmp_file_str = strcat(tmp_file_str, ".csv");
      tmp_file = fopen(tmp_file_str, 'w');
      fputs(tmp_file, strcat(tmp_str, "\n"));

      csvwrite( tmp_file, ...
        [xvalues', tmp_mat]);
      fclose(tmp_file);
end
clear tmp_file_str; 
clear tmp_file;
clear tmp_mat;
clear tmp_str;
end