/*
 * Waveguide reference solution in full 3D.
 * 
 */

Include "../shared_pro.dat";
Include "../shared_functions.dat";

DefineConstant[
    ansatz = {(m>0?2:1), Name "Input/05Q3D/Ansatz",  Choices{1 = "none", 2 = "e_phi_star"}}
    //used_iterations = -1
];

Group{
    // set of the indices of all subdomains
    
    Dummy = Region[{10000}];
    Domains() = {};
    For dom_idx In {0:no_of_subdomains-1}
        Domains() += dom_idx;
        Vol~{dom_idx} = Region[{(dom_idx+1)}];
        
        Sur_lower~{dom_idx} = Region[{(100*(dom_idx+1)+1)}];
        Sur_upper~{dom_idx} = Region[{(100*(dom_idx+1)+2)}];
        Sur_outer~{dom_idx} = Region[{(100*(dom_idx+1)+3)}];
        Sur_inner~{dom_idx} = Region[{(100*(dom_idx+1)+4)}];
        
        // Part of boundary where homogenous Dirichlet conditions apply
        Gamma_D0~{dom_idx} = Region[{Sur_outer~{dom_idx}}];
        
        // Symmetry axis
        Gamma_Symm~{dom_idx} = Region[{Sur_inner~{dom_idx}}];
        
        // Part of boundary where non-homogenous Dirichlet conditions apply (excitation)
        Gamma_D~{dom_idx} = Region[{}];
        If (dom_idx == 0)
            Gamma_D~{dom_idx} += Region[{Sur_lower~{dom_idx}}];
        EndIf
        
        // Part of boundary where infinite domain conditions apply
        Gamma_Inf~{dom_idx} = Region[{}];
        If (dom_idx == no_of_subdomains-1)
            Gamma_Inf~{dom_idx} += Region[{Sur_upper~{dom_idx}}];
        EndIf
        
        // Part of boundary where Neumann conditions apply
        Gamma_N~{dom_idx} = Region[{}]; //not used
        
        // set neighborhood information and interfaces Sigma_ij
        If (dom_idx == 0)
            // only right neighbor
            If (no_of_subdomains > 1) // fix to work with edge case of 1 subdomain
                Domains~{dom_idx} = {dom_idx+1};
                Sigma~{dom_idx}~{dom_idx+1} = Region[{Sur_upper~{dom_idx}}];
            Else
                Domains~{dom_idx} = {};
                Sigma~{dom_idx}~{dom_idx+1} = Region[{}];
            EndIf
            Sigma~{dom_idx} = Region[{Sigma~{dom_idx}~{dom_idx+1}}];
            BndSigma~{dom_idx} = Region[{}];
            BndSigma~{dom_idx}~{dom_idx+1} = Region[{BndSigma~{dom_idx}}];
        ElseIf (dom_idx == no_of_subdomains-1)
            // only left neighbor
            If (no_of_subdomains > 1)
                Domains~{dom_idx} = {dom_idx-1};
                Sigma~{dom_idx}~{dom_idx-1} = Region[{Sur_lower~{dom_idx}}];
            Else
                Domains~{dom_idx} = {};
                Sigma~{dom_idx}~{dom_idx-1} = Region[{}];
            EndIf
            Sigma~{dom_idx} = Region[{Sigma~{dom_idx}~{dom_idx-1}}];
            BndSigma~{dom_idx} = Region[{}];
            BndSigma~{dom_idx}~{dom_idx-1} = Region[{BndSigma~{dom_idx}}];
        Else
            // left and right neighbor
            Domains~{dom_idx} = {dom_idx-1, dom_idx+1};
            Sigma~{dom_idx}~{dom_idx+1} = Region[{Sur_upper~{dom_idx}}];
            Sigma~{dom_idx}~{dom_idx-1} = Region[{Sur_lower~{dom_idx}}];
            Sigma~{dom_idx} = Region[{Sigma~{dom_idx}~{dom_idx+1}, Sigma~{dom_idx}~{dom_idx-1}}];
            BndSigma~{dom_idx}~{dom_idx+1} = Region[{}];
            BndSigma~{dom_idx}~{dom_idx-1} = Region[{}];
            BndSigma~{dom_idx} = Region[{BndSigma~{dom_idx}~{dom_idx+1}, BndSigma~{dom_idx}~{dom_idx-1}}];
        EndIf        
    EndFor
    
    // port for excitation
    Port_1 = Region[{Gamma_D_0}];
    
    //initilize iterations_used
    iterations_used = -1;
}

DefineConstant[
    convergence_output_filename={"convergence_study.log"},
    execute_error_calc = {0, Choices{0, 1}, Name "Perform error calculation"},
    solver={"gmres", Choices {"gmres"}, Name "Iterative Solver/Solver"},
    tolerance = {1e-05, Min 1e-16, Max 1e-1, Name "Iterative Solver/Tolerance"},
    max_iterations = {5000, Min 1, Max 1e5, Name "Iterative Solver/Max. Iterations"}, 
    restart = max_iterations,
    R_ = {"schwarz_ddm", Name "GetDP/1ResolutionChoices", Visible 0},
    C_ = {"-solve -v 2 -bin -ksp_monitor", Name "GetDP/9ComputeCommand", Visible 0},
    P_ = {"", Name "GetDP/2PostOperationChoices", Visible 0}
];

If (exctype==1) mode_type_str = "TE";
ElseIf (exctype==2) mode_type_str = "TM";
EndIf
If (isCavity==1) cavity_str = "cavity";
ElseIf (isCavity==0) cavity_str = "non-cavity";
EndIf
If (highOrder == 1) order_str = "high order";
ElseIf (highOrder == 0) order_str = "regular";
EndIf
If (m_sign == -1) mode_sign_str = "-m";
ElseIf (m_sign == 1) mode_sign_str = "+m";
EndIf

Function{ 
    // cyl. coordinates
    r[] = X[];
    z[] = Y[];
    
    //unit vectors of cyl. coordinates
    unitPhi[] = Vector[0, 0, -1];
    unitR[] = Vector[1, 0, 0];
    unitZ[] = Vector[0, 1, 0];
    
    // complex unit
    I[] = Complex[0., 1.];
    
    // wave number
    k[] = k;
    
    // used for TC
    kIBC[] = k[];
    
    //Excitation
    If (exctype==1) //TE
        // pozar. A and B can be chosen freely in this formulation, but not in the 2.5D formulation.
        f_exc_rz [Port_1] = (-I[] * omega * mu[] * m) / (k_r^2 * r[]) * (A - B) * Jn[m,k_r * r[]] * unitR[];
        f_exc_phi [Port_1] = (I[] * omega * mu[]) / k_r * (A + B) * dJn[m, k_r * r[]];
        mode_type_str = "TE";
    ElseIf (exctype==2) //TM
        // pozar. A and B can be chosen freely in this formulation, but not in the 2.5D formulation.
        f_exc_rz [Port_1] = (A + B) * Jn[m, k_r * r[]] * unitZ[] 
                      + (-I[] * k_z / k_r) * (A + B) * dJn[m, k_r * r[]] * unitR[];
        f_exc_phi [Port_1] = (-I[] * k_z * m) / (k_r^2 * r[]) * (A - B) * Jn[m, k_r * r[]];
        mode_type_str = "TM";
    EndIf 
    
    // analytical solution
    If (exctype==1) //TE
        f_ana_rz[] = - I[] * omega * mu[] * m / (k_r^2 * r[]) * (A - B) * Jn[m, k_r * r[]]* Exp[-I[] * k_z * z[]] * unitR[];
        f_ana_phi[] = I[] * omega * mu[] / k_r * (A + B) * dJn[m, k_r * r[]] * Exp[-I[] * k_z * z[]];
    ElseIf (exctype==2) //TM
        f_ana_rz[] = - I[] * k_z / k_r * (A + B) * dJn[m, k_r * r[]] * Exp[-I[] * k_z * z[]] * unitR[]
                  + (A + B) * Jn[m, k_r * r[]] * Exp[-I[] * k_z * z[]] * unitZ[];
        f_ana_phi[] = - I[] * k_z * m / (k_r^2 * r[]) * (A - B) * Jn[m, k_r * r[]] * Exp[-I[] * k_z * z[]];
    EndIf
    
    // Pade-type TC
    NP_OSRC = 8;
    kepsI = 0.;
    keps[] = k[]*(1+kepsI*I[]);
    theta_branch = Pi/4;
}

Include "Decomposition.pro"; // ListOfField, ListOfConnectedFields, initializing g_i_j
Include "Maxwell.pro"; // Jacobian, Integration, Constraint, Functionspace, Formulation, PostProcessing, PostOperation
Include "Schwarz.pro"; // Resolution
