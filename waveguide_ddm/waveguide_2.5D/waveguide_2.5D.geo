/*
 * Waveguide 3D DDM
 */

Include "../shared_pro.dat";

Point (100000) = {0,0,0}; // dummy
Physical Point (100000) = {100000};

For i In {0:no_of_subdomains-1}
    // Points
    Point(10*i + 1) = {0,i*l,0,res}; 
    Point(10*i + 2) = {R,i*l,0,res};
    Point(10*i + 3) = {R,(i+1)*l,0,res};
    Point(10*i + 4) = {0,(i+1)*l,0,res};
    
    // Lines
    Line(10*i + 1) = {10*i + 1, 10*i + 2};
    Line(10*i + 2) = {10*i + 2, 10*i + 3};
    Line(10*i + 3) = {10*i + 3, 10*i + 4};
    Line(10*i + 4) = {10*i + 4, 10*i + 1};
    
    // Surface
    Curve Loop (10*i + 5) = {10*i + 1, 10*i + 2, 10*i + 3, 10*i + 4};
    Plane Surface (10*i + 6) = {10*i + 5};
    
    Physical Line (100*(i+1)+1) = {10*i + 1}; // left, z=i*l
    Physical Line (100*(i+1)+2) = {10*i + 3}; // right, z=(i+1)*l
    Physical Line (100*(i+1)+3) = {10*i + 2}; // outer, r=R
    Physical Line (100*(i+1)+4) = {10*i + 4}; // inner, r=0
    Physical Surface(i+1) = {10*i + 6};
EndFor

Transfinite Surface "*";
