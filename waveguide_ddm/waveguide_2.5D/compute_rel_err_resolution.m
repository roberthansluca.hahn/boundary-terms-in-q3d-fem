function compute_rel_err_waveguide(m_max=2, n_max=3, start_res=5, res_steps=5, print_figures=false, ...
  source_file='convergence_waveguide/low_order/convergence_study_raw', target_file='convergence_waveguide/low_order/convergence_waveguide_results', figure_dest='../../../figures/convergence/ddm/Q3D/low_order/')
data=load(source_file)(:, 2); # load source file. Only take 2nd column (real part of L2-Norm)
idx=1; # starting index
subdomain_count = 4;
idx_step=4*subdomain_count; # 4 lines for each mode (inplane and outofplane, err. and analytical solution)
#rel_errs = zeros(2, m_max+1, n_max, subdomain_count, 2, res_steps+1); # save rel erros here. 
errs_subdomain = zeros(2, m_max+1, n_max, subdomain_count, 3, res_steps+1);
norms_subdomain = zeros(2, m_max+1, n_max, subdomain_count, 3, res_steps+1);
# Index scheme is {1 for TE, 2 for TM}, {m+1}, {n}, {subdomain index}, {1 for inplane, 2 for outofplane, 3 for combined}, {refinement step}
for type=1:2
    for m=0:m_max
        for n=1:n_max
            for s=1:subdomain_count
                # extract squared err norm, squared analytical solution for in- and outofplane 
                squ_err_inplane = data([idx:idx_step:idx+idx_step*res_steps]);
                squ_norm_inplane = data([idx+1:idx_step:idx+idx_step*res_steps+1]);
                squ_err_outofplane = data([idx+2:idx_step:idx+idx_step*res_steps+2]);
                squ_norm_outofplane = data([idx+3:idx_step:idx+idx_step*res_steps+3]);
                #
                errs_subdomain(type, m+1, n, s, 1, :) = squ_err_inplane;
                norms_subdomain(type, m+1, n, s, 1, :) = squ_norm_inplane;
                errs_subdomain(type, m+1, n, s, 2, :) = squ_err_outofplane;
                norms_subdomain(type, m+1, n, s, 2, :) = squ_norm_outofplane;
                errs_subdomain(type, m+1, n, s, 3, :) = squ_err_outofplane + squ_err_inplane;
                norms_subdomain(type, m+1, n, s, 3, :) = squ_norm_outofplane + squ_norm_inplane;
                idx += subdomain_count;
            end
            idx+=idx_step*res_steps; # step to beginning of next mode
        end
    end
end

xvalues = start_res .* 2.^[0:res_steps];
### slope
convergence_slope = @(xval, yval) ...
(log2(yval(2:end)) - log2(yval(1:end-1))) ./ (log2(xval(2:end)) - log2(xval(1:end-1)));

# 1) compute relative error for solution of whole domain.
errs_whole_domain = zeros(2, m_max+1, n_max, 3, res_steps+1);
norms_whole_domain = zeros(2, m_max+1, n_max, 3, res_steps+1);
for s=1:subdomain_count
    errs_whole_domain(:, :, :, :, :) += squeeze(errs_subdomain(:, :, :, s, :, :));
    norms_whole_domain(:, :, :, :, :) += squeeze(norms_subdomain(:, :, :, s, :, :));
end
rel_err_whole_domain = sqrt(errs_whole_domain) ./ sqrt(norms_whole_domain);

#
### save results in csv files
# 

for mode_type =1:2
    for plane=1:3
      tmp_mat = zeros(res_steps+1, m_max+1 + n_max);
      tmp_str = "elements";
      for m=0:m_max
        for n=1:n_max
          tmp_mat(:, m*n_max+n) = rel_err_whole_domain(mode_type, m+1, n, plane, :);
          tmp_str = strcat(tmp_str, sprintf(",m=%g-n=%g",m,n));
        end
      end
      tmp_file_str = strcat(figure_dest, "/raw/waveguide");
      if (mode_type == 1) tmp_file_str = strcat(tmp_file_str, "_te"); else tmp_file_str = strcat(tmp_file_str, "_tm"); end
      if (plane == 1) tmp_file_str = strcat(tmp_file_str, "_inplane"); 
      elseif (plane == 2) tmp_file_str = strcat(tmp_file_str, "_outofplane"); 
      else tmp_file_str = strcat(tmp_file_str, "_combined");
      end
      tmp_file_str = strcat(tmp_file_str, ".csv");
      tmp_file = fopen(tmp_file_str, 'w');
      fputs(tmp_file, strcat(tmp_str, "\n"));

      csvwrite( tmp_file, ...
        [xvalues', tmp_mat]);
      fclose(tmp_file);
    end
end
clear tmp_file_str; 
clear tmp_file;
clear tmp_mat;
clear tmp_str;
end