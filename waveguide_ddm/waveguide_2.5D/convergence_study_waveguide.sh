#!/bin/bash

###
###	user defined arguments
###

total_length=0.2		#length of the waveguide
radius=0.035			#radius of the waveguide
radial_freq=1e+11		#radial frequency at which the field is excited
number_of_subdomains_start=2	#number of subdomains to start at
number_of_subdomains_steps=5	#subdomain count is doubled in each refinement step
ele_per_lambda_start=5		#number of elements per wavelength to start at.
ele_per_lambda_steps=6		#number of convergence data points (number of refinement steps + 1)
complete_basis=1		#when true, BF_Edge_2E etc are included.

###	files

log_file_name="convergence_study.log"	#PostOperation writes results here
raw_data_file="convergence_study_raw"	#awk scrapes log_file_name and writes results here
problem_base_name="waveguide_2.5D"	#base name of .pro, .geo, etc.

###	Operations to perform
op_solve=1	# call getdp to solve, generate logs
op_scrape=1	# scrape existing logs to recover raw numbers
op_process=1	# process raw numbers with octave
refine_resolution=1 # perform resolution refinement if 1, perform subdomain refinement if 0


directory=$(dirname "$(readlink -f "$0")")	#directory of this script
if [[ complete_basis -eq 1 ]]
then
	order_dir=high_order
else
	order_dir=low_order
fi

if [[ $refine_resolution -eq 1 ]]
then
    refinement_str=resolution
else
    refinement_str=subdomains
fi

# decreasing refinement steps by one to avoid arithmetic later on
let ele_per_lambda_steps=$ele_per_lambda_steps-1
let number_of_subdomains_steps=$number_of_subdomains_steps-1

if [[ $op_solve -eq 1 ]]
then

rm -f $directory/convergence_waveguide/$refinement_str/$order_dir/$log_file_name # remove old logs
echo "Starting convergence study of Q3D DDM at "$(date) >> $directory/convergence_waveguide/$refinement_str/$order_dir/$log_file_name

for mode_type in 1 2 # 1 for TE, 2 for TM
do
	for m in 0 1 2
	do
		for n in 1 2 3
		do
            if [[ $refine_resolution -eq 0 ]]
            then
			for number_of_subdomains in $(octave-cli --eval "disp($number_of_subdomains_start.*2.^[0:$number_of_subdomains_steps]);")
			do
			ele_per_lambda=20
			#ansatz
					if (($m > 0))
					then
						ansatz=2	# e-phi-star
					else
						ansatz=1	# no ansatz
					fi

					#mode sign
					if [[ $mode_type -eq 1 ]]
					then
						mode_sign=-1
					else
						mode_sign=1
					fi

					# argument strings
					gmsh_args="-setnumber L $total_length -setnumber R $radius -setnumber no_of_ele_per_lambda $ele_per_lambda -setnumber no_of_subdomains $number_of_subdomains" # -setnumber omega $radial_freq"
					
					getdp_args=$gmsh_args" -setnumber highOrder $complete_basis -setnumber exctype $mode_type -setnumber m_sign $mode_sign -setnumber m $m -setnumber n $n -setnumber ansatz $ansatz -setnumber output_convergence_logs 0 -setnumber execute_error_calc 1 -setstring convergence_output_filename $directory/convergence_waveguide/$refinement_str/$order_dir/$log_file_name"
					# remove old mesh, database and output files
					rm -f $directory/$problem_base_name.msh $directory/$problem_base_name.db $directory/out/*

					# generate mesh
					gmsh $directory/$problem_base_name.geo $gmsh_args -2 	# mesh 2D, save and exit

					# solve
					getdp $directory/$problem_base_name.pro $getdp_args -solve schwarz_ddm 
			done
	    fi
	    if [[ $refine_resolution -eq 1 ]]
            then 
            		for ele_per_lambda in $(octave-cli --eval "disp($ele_per_lambda_start.*2.^[0:$ele_per_lambda_steps]);")
            		do
			number_of_subdomains=4
                	#ansatz
                	if (($m > 0))
                then
                    ansatz=2	# e-phi-star
                else
                    ansatz=1	# no ansatz
                fi

                #mode sign
                if [[ $mode_type -eq 1 ]]
                then
                    mode_sign=-1
                else
                    mode_sign=1
                fi
                # argument strings
                gmsh_args="-setnumber L $total_length -setnumber R $radius -setnumber no_of_ele_per_lambda $ele_per_lambda -setnumber no_of_subdomains $number_of_subdomains" # -setnumber omega $radial_freq"
                        
                getdp_args=$gmsh_args" -setnumber highOrder $complete_basis -setnumber exctype $mode_type -setnumber m_sign $mode_sign -setnumber m $m -setnumber n $n -setnumber ansatz $ansatz -setnumber output_convergence_logs 0 -setnumber execute_error_calc 1 -setstring convergence_output_filename $directory/convergence_waveguide/$refinement_str/$order_dir/$log_file_name"
                # remove old mesh, database and output files
                rm -f $directory/$problem_base_name.msh $directory/$problem_base_name.db $directory/out/*

                # generate mesh
                gmsh $directory/$problem_base_name.geo $gmsh_args -2 	# mesh 2D, save and exit

                # solve
                getdp $directory/$problem_base_name.pro $getdp_args -solve schwarz_ddm 
	    done
            fi
		done
	done
done

echo -e "Convergence study of Q3D DDM ended at "$(date)"\n\n=================================================================================================\n" >> $directory/convergence_waveguide/$refinement_str/$order_dir/$log_file_name
fi

# scrape log for raw numbers
if [[ $op_scrape -eq 1 ]]
then

rm -f $directory/convergence_waveguide/$refinement_str/$order_dir/$raw_data_file
awk '/^0 * */ {print $0}' $directory/convergence_waveguide/$refinement_str/$order_dir/$log_file_name >> $directory/convergence_waveguide/$refinement_str/$order_dir/$raw_data_file

fi

# process raw numbers in octave
if [[ $op_process -eq 1 ]]
then
	if [[ $refine_resolution -eq 1 ]] 
	then
	octave --path $directory --eval "compute_rel_err_resolution(2, 3, $ele_per_lambda_start, $ele_per_lambda_steps, 1, '$directory/convergence_waveguide/$refinement_str/$order_dir/$raw_data_file', '$directory/convergence_waveguide/$refinement_str/$order_dir/convergence_results', '$directory/../../../figures/convergence/ddm/Q3D/$refinement_str/$order_dir/');"
	else
	octave --path $directory --eval "compute_rel_err_subdomains(2, 3, $number_of_subdomains_start, $number_of_subdomains_steps, 1, '$directory/convergence_waveguide/$refinement_str/$order_dir/$raw_data_file', '$directory/convergence_waveguide/$refinement_str/$order_dir/convergence_results', '$directory/../../../figures/convergence/ddm/Q3D/$refinement_str/$order_dir/');"
	fi
fi
