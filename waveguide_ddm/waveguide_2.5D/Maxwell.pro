Jacobian { // transformation to ref. element. vol means the regular transformation
    { Name JacVol; Case{ { Region All; Jacobian Vol;}}}
    { Name JacSur; Case{{Region All; Jacobian Sur;}}}
    { Name JacLin; Case{{Region All; Jacobian Lin;}}}
}

IntPointsLine = 8;
IntPointsTri = 16;

Integration {
    { Name Int;
        Case{   
            { Type Gauss;
                    Case { 
                        {GeoElement Point; NumberOfPoints 1;}
                        {GeoElement Triangle;   NumberOfPoints IntPointsTri;} //16
                        {GeoElement Line ;      NumberOfPoints IntPointsLine;} //8
                        {GeoElement Tetrahedron; NumberOfPoints 4;}
                    }
            }
        }
    }
}

Group{
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            BndSigmaInf~{i}~{j} = Region [BndSigma~{i}~{j}, Not {Gamma_N~{i}, Gamma_D~{i}}];
        EndFor
    EndFor    
}

Constraint{
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        {Name e_Constraint_PEC~{i}; // Dirichlet Boundary condition on the PEC parts of the waveguide
            Case {
                {Region Gamma_D0~{i}; Type Assign; Value 0.;}
            }    
        }
        
        // dirichlet on symmetry axis, out-of-plane
        { Name e_phi_star_symm~{i};
            Case{
                {Region Gamma_Symm~{i}; Type Assign; Value 0.;}
            }
        }
        
        { Name e_phi_symm~{i};
            Case{
                {Region Gamma_Symm~{i}; Type Assign; Value 0.;}
            }
        }
        
        // dirichlet on symmetry axis, in-plane
        { Name e_rz_symm~{i};
            Case{
                {Region Gamma_Symm~{i}; Type Assign; Value 0.;}
            }
        }
    EndFor
}

FunctionSpace {
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        // electric field in each subdomain
        {Name Hcurl_e_rz~{i}; Type Form1;
            BasisFunction{
                {Name basis_erz; NameOfCoef erz_coeff; Function BF_Edge;
                    Support Region[{Vol~{i}, Gamma_D~{i}, Gamma_Inf~{i}, Sigma~{i}, Gamma_D0~{i}}]; 
                    Entity EdgesOf[All];}
                If (highOrder == 1)
                    {Name basis_erz2; NameOfCoef erz_coeff2; Function BF_Edge_2E;
                    Support Region[{Vol~{i}, Gamma_D~{i}, Gamma_Inf~{i}, Sigma~{i}, Gamma_D0~{i}}]; 
                    Entity EdgesOf[All];}
                EndIf
            }
            Constraint {
                {   NameOfCoef erz_coeff; EntityType EdgesOf;
                    NameOfConstraint e_Constraint_PEC~{i};
                }
                If (m != 0)
                    {   NameOfCoef erz_coeff; EntityType EdgesOf;
                        NameOfConstraint e_rz_symm~{i};
                    }
                EndIf
                If (highOrder == 1)
                    {   NameOfCoef erz_coeff2; EntityType EdgesOf;
                        NameOfConstraint e_Constraint_PEC~{i};
                    }
                    If (m != 0)
                        {   NameOfCoef erz_coeff2; EntityType EdgesOf;
                            NameOfConstraint e_rz_symm~{i};
                        }
                    EndIf
                EndIf
            }
        }
        {Name H1_e_phi~{i}; Type Form0;
            // out-of-plane component for m=0
            BasisFunction{
                { Name basis_ephi; NameOfCoef ephi_coeff; Function BF_Node;
                    Support Region[{Vol~{i}, Gamma_D~{i}, Gamma_Inf~{i}, Sigma~{i}, Gamma_D0~{i}}]; 
                    Entity NodesOf[All];}
                If (highOrder == 1)
                    { Name basis_ephi2; NameOfCoef ephi_coeff2; Function BF_Node_2E;
                        Support Region[{Vol~{i}, Gamma_D~{i}, Gamma_Inf~{i}, Sigma~{i}, Gamma_D0~{i}}]; 
                        Entity EdgesOf[All];}
                EndIf
            }
            Constraint {
                    If (ansatz==1)
                        {   NameOfCoef ephi_coeff; EntityType NodesOf;
                            NameOfConstraint e_Constraint_PEC~{i};}
                        If (highOrder == 1)
                            {   NameOfCoef ephi_coeff2; EntityType EdgesOf;
                                NameOfConstraint e_Constraint_PEC~{i};}
                        EndIf
                    EndIf
                    {   NameOfCoef ephi_coeff; EntityType NodesOf;
                        NameOfConstraint e_phi_symm~{i};}
                    If (highOrder == 1)
                        {   NameOfCoef ephi_coeff2; EntityType EdgesOf;
                            NameOfConstraint e_phi_symm~{i};}
                    EndIf
            }
        }
        {Name H1_e_phi_star~{i}; Type Form0;
            BasisFunction{
                { Name basis_ephistar; NameOfCoef ephistar_coeff; Function BF_Node;
                    Support Region[{Vol~{i}, Gamma_D~{i}, Gamma_Inf~{i}, Sigma~{i}, Gamma_D0~{i}}]; 
                    Entity NodesOf[All];}
                If (highOrder == 1)
                    { Name basis_ephistar2; NameOfCoef ephistar_coeff2; Function BF_Node_2E;
                        Support Region[{Vol~{i}, Gamma_D~{i}, Gamma_Inf~{i}, Sigma~{i}, Gamma_D0~{i}}]; 
                        Entity EdgesOf[All];} 
                EndIf
            }
            Constraint {
                If (ansatz == 2)
                    { NameOfCoef ephistar_coeff; EntityType NodesOf;
                        NameOfConstraint e_Constraint_PEC~{i};
                    }
                    { NameOfCoef ephistar_coeff; EntityType NodesOf;
                        NameOfConstraint e_phi_star_symm~{i};
                    }
                    If (highOrder == 1)
                        { NameOfCoef ephistar_coeff2; EntityType EdgesOf;
                            NameOfConstraint e_Constraint_PEC~{i};
                        }
                        { NameOfCoef ephistar_coeff2; EntityType EdgesOf;
                            NameOfConstraint e_phi_star_symm~{i};
                        }                    
                    EndIf
                EndIf
            }
        }
                
        // lagrange multipliers
        {Name Hcurl_lambda~{i}; Type Form1;
            BasisFunction{
                { Name basis_lambda_rz; NameOfCoef lambdarz_coeff; Function BF_Edge;
                    Support Region[{Gamma_D~{i}}]; Entity EdgesOf[All];}
                If (highOrder == 1)
                    { Name basis_lambda_rz2; NameOfCoef lambdarz2_coeff; Function BF_Edge_2E;
                        Support Region[{Gamma_D~{i}}]; Entity EdgesOf[All];}
                EndIf
            }
            Constraint{
                { NameOfCoef lambdarz_coeff; EntityType EdgesOf; NameOfConstraint e_Constraint_PEC~{i};}
                If (highOrder == 1)
                            { NameOfCoef lambdarz2_coeff; EntityType EdgesOf; NameOfConstraint e_Constraint_PEC~{i};}
                EndIf
                }
        }
        {Name H1_lambda~{i}; Type Form0;
            BasisFunction{
                { Name basis_lambda_phi; NameOfCoef lambdaphi_coeff; Function BF_Node;
                    Support Region[{Gamma_D~{i}}]; Entity NodesOf[All];}
                If (highOrder == 1)
                            { Name basis_lambda_phi2; NameOfCoef lambdaphi2_coeff; Function BF_Node_2E; Support Region[{Gamma_D~{i}}]; Entity EdgesOf[All];}
                EndIf
                }
            Constraint{
                { NameOfCoef lambdaphi_coeff; EntityType NodesOf; NameOfConstraint e_Constraint_PEC~{i};}
            	If (highOrder == 1)
                	{ NameOfCoef lambdaphi2_coeff; EntityType EdgesOf; NameOfConstraint e_Constraint_PEC~{i};}
                EndIf
            }
        }        
        
        // interface residuals g_ij on Sigma_ij
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            { Name Hcurl_g_out~{i}~{j}; Type Form1;
              BasisFunction {
                  { Name basis_g_rz; NameOfCoef grz_coeff; Function BF_Edge;
                      Support Region[{Sigma~{i}~{j}}]; 
                      Entity EdgesOf[Sigma~{i}~{j}, Not {Gamma_D~{i}, Gamma_D0~{i}, Gamma_Inf~{i}}];}
                    If (highOrder == 1)
                    { Name basis_g_rz2; NameOfCoef grz2_coeff; Function BF_Edge_2E;
                      Support Region[{Sigma~{i}~{j}}]; 
                      Entity EdgesOf[Sigma~{i}~{j}, Not {Gamma_D~{i}, Gamma_D0~{i}, Gamma_Inf~{i}}];}
                    EndIf
                } 
              }
            { Name H1_g_out~{i}~{j}; Type Form0;
              BasisFunction {
                  { Name basis_g_phi; NameOfCoef gphi_coeff; Function BF_Node;
                      Support Region[{Sigma~{i}~{j}}]; 
                      Entity NodesOf[Sigma~{i}~{j}, Not {Gamma_D~{i}, Gamma_D0~{i}, Gamma_Inf~{i}}];}
                If (highOrder == 1)
                    { Name basis_g_phi2; NameOfCoef gphi2_coeff; Function BF_Node_2E;
                        Support Region[{Sigma~{i}~{j}}]; 
                        Entity EdgesOf[Sigma~{i}~{j}, Not {Gamma_D~{i}, Gamma_D0~{i}, Gamma_Inf~{i}}];}
                EndIf
              }
            }
        EndFor
        
        // for Pade TC
        
        If (tc_type == 2)
            For jj In {0:#myDomains~{i}()-1}
                j = myDomains~{i}(jj);
                { Name Hcurl_r~{i}~{j}; Type Form1;
                    BasisFunction {
                        {   Name ser1; NameOfCoef eer1; Function BF_Edge;
                            Support Region[{Sigma~{i}~{j}}];
                            Entity EdgesOf[Sigma~{i}~{j}, Not {Gamma_D~{i}, Gamma_D0~{i}, Gamma_Inf~{i}}];
                        }
                    }
                }
                For h In {1:NP_OSRC}
                    { Name Hcurl_phi~{h}~{i}~{j}; Type Form1;
                        BasisFunction {
                        { Name sph1; NameOfCoef eph1; Function BF_Edge;
                            Support Region[{Sigma~{i}~{j}}];
                            Entity EdgesOf[Sigma~{i}~{j},
                                        Not {Gamma_D~{i}, Gamma_D0~{i}, Gamma_Inf~{i}}]; }
                        }
                    }
                    { Name Hgrad_rho~{h}~{i}~{j}; Type Form0;
                        BasisFunction {
                        { Name srh1; NameOfCoef erh1; Function BF_Node;
                            Support Region[{Sigma~{i}~{j}}];
                            Entity NodesOf[Sigma~{i}~{j},
                                        Not {Gamma_D~{i}, Gamma_D0~{i}, Gamma_Inf~{i}}]; }
                        }
                    }
                EndFor
            EndFor
        EndIf
    EndFor
}


Formulation {
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        If (ansatz == 1)
        {Name Vol_Formulation_1~{i}; Type FemEquation;
            Quantity {
                { Name e_rz~{i}; Type Local; NameOfSpace Hcurl_e_rz~{i}; }
                { Name e_phi~{i}; Type Local; NameOfSpace H1_e_phi~{i};}
                { Name lambda_rz~{i}; Type Local; NameOfSpace Hcurl_lambda~{i};}
                { Name lambda_phi~{i}; Type Local; NameOfSpace H1_lambda~{i};}
                // Pade TC
                
                If(tc_type == 2)
                    For jj In {0:#myDomains~{i}()-1}
                    j = myDomains~{i}(jj);
                    { Name r_rz~{i}~{j}; Type Local; NameOfSpace Hcurl_r~{i}~{j};}
                    { Name r_phi~{i}~{j}; Type Local; NameOfSpace Hcurl_r~{i}~{j};}
                        For h In {1:NP_OSRC}
                            { Name phi_rz~{h}~{i}~{j}; Type Local; NameOfSpace Hcurl_phi~{h}~{i}~{j};}
                            { Name phi_phi~{h}~{i}~{j}; Type Local; NameOfSpace Hcurl_phi~{h}~{i}~{j};}
                            { Name rho_rz~{h}~{i}~{j}; Type Local; NameOfSpace Hgrad_rho~{h}~{i}~{j};}
                            { Name rho_phi~{h}~{i}~{j}; Type Local; NameOfSpace Hgrad_rho~{h}~{i}~{j};}
                        EndFor
                    EndFor
                EndIf
            }
            If (isCavity == 0) // waveguide, direct problem
                Equation {
                    // volume problem
                    Integral { [nu_r[] * r[] * Dof{d e_rz~{i}}, {d e_rz~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }
                    Integral { [- eps_r[] * r[] * omega^2 / c[]^2 * Dof{e_rz~{i}}, {e_rz~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }
                    Integral { [- eps_r[] * r[] * omega^2 / c[]^2 * Dof{e_phi~{i}}, {e_phi~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }
                    Integral { [nu_r[] / r[] * Dof{e_phi~{i}}, {e_phi~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }    
                    Integral { [nu_r[] * Dof{e_phi~{i}} * unitR[], {d e_phi~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }   
                    Integral { [nu_r[] * Dof{d e_phi~{i}} * unitR[], {e_phi~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }       
                    Integral { [nu_r[] * r[] * Dof{d e_phi~{i}} * unitR[], {d e_phi~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }     
                    // ABC (0-th order sommerfeld)
                    Integral { [I[] * nu_r[] * r[] * k_inf * Dof{e_rz~{i}} , {e_rz~{i}} ]; 
                        In Gamma_Inf~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [I[] * nu_r[] * r[] * k_inf * Dof{e_phi~{i}} , {e_phi~{i}} ]; 
                        In Gamma_Inf~{i}; Jacobian JacSur; Integration Int; }  
                        
                    // artificial sources (g_in)
                    
                    For jj In {0:#myDomains~{i}()-1}
                        j = myDomains~{i}(jj);
                        Integral{[$ArtificialSource~{j} ?  g_in_rz~{i}~{j}[] : Vector[0,0,0], {e_rz~{i}}]; 
                            In Sigma~{i}~{j}; Jacobian JacSur; Integration Int;}
                        Integral{[$ArtificialSource~{j} ?  g_in_phi~{i}~{j}[] : 0, {e_phi~{i}}]; 
                            In Sigma~{i}~{j}; Jacobian JacSur; Integration Int;}
                    EndFor
                    
                    // excitation using lagrange multipliers
                    // in-plane
                    
                    Integral{ [Dof{lambda_rz~{i}}, {e_rz~{i}}];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int;}
                    Integral { [ 0*Dof{lambda_rz~{i}} , {lambda_rz~{i}} ]; // don't remove this
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [ Dof{e_rz~{i}} , {lambda_rz~{i}} ];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [ ($PhysicalSource ? -f_exc_rz[] 
                        : Vector[0,0,0]), {lambda_rz~{i}} ];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    
                     // out-of-plane
                    Integral{ [Dof{lambda_phi~{i}}, {e_phi~{i}}];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int;}
                    Integral { [ 0*Dof{lambda_phi~{i}} , {lambda_phi~{i}} ]; // don't remove this
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [ Dof{e_phi~{i}} , {lambda_phi~{i}} ];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [ ($PhysicalSource ? -f_exc_phi[] 
                        : Vector[0,0,0]), {lambda_phi~{i}} ];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                        
                    // transmission condition
                    If (tc_type == 0) // zeroth-order
                        For jj In {0:#myDomains~{i}()-1}
                            j = myDomains~{i}(jj);
                            Integral{[- I[] * r[] * kIBC[] * Dof{e_rz~{i}}, {e_rz~{i}}];
                                In Sigma~{i}~{j}; Jacobian JacSur; Integration Int;}
                            Integral{[- I[] * r[] * kIBC[] * Dof{e_phi~{i}}, {e_phi~{i}}];
                                In Sigma~{i}~{j}; Jacobian JacSur; Integration Int;}
                        EndFor
                    EndIf
                    
                    /*
                    If(tc_type == 2)
                        For jj In {0:#myDomains~{i}()-1}
                            j = myDomains~{i}(jj);
                            Integral { [ -I[] * k[] * r[] * Dof{r_rz~{i}~{j}} , {e_rz~{i}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                            Integral { [ -I[] * k[] * r[] * Dof{r_phi~{i}~{j}} , {e_phi~{i}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                
                            Integral { [ - Dof{e_rz~{i}} * r[], {r_rz~{i}~{j}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                            Integral { [ - Dof{e_phi~{i}} * r[], {r_phi~{i}~{j}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }   
                                
                            Integral { [ 1/keps[]^2 * r[] * Dof{d e_rz~{i}} , {d r_rz~{i}~{j}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                            Integral { [ 1/keps[]^2 * r[] * Dof{d e_phi~{i}} , {d r_phi~{i}~{j}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }   
                                
                            Integral { [ OSRC_C0[]{NP_OSRC,theta_branch} * r[] * Dof{r_rz~{i}~{j}},
                                {r_rz~{i}~{j}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                            Integral { [ OSRC_C0[]{NP_OSRC,theta_branch} * r[] * Dof{r_phi~{i}~{j}},
                                {r_phi~{i}~{j}} ];    
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                            
                            For h In{1:NP_OSRC}
                                Integral { [ OSRC_Aj[]{h,NP_OSRC,theta_branch} * r[] * 
                                    Dof{d rho_rz~{h}~{i}~{j}}, {r_rz~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                Integral { [ OSRC_Aj[]{h,NP_OSRC,theta_branch} * r[] *
                                    Dof{d rho_phi~{h}~{i}~{j}}, {r_phi~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }   
                                    
                                Integral { [ -OSRC_Aj[]{h,NP_OSRC,theta_branch} / keps[]^2 * r[] *
                                    Dof{d phi_rz~{h}~{i}~{j}}, {d r_rz~{i}~{j}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                Integral { [ -OSRC_Aj[]{h,NP_OSRC,theta_branch} / keps[]^2 * r[] *
                                    Dof{d phi_phi~{h}~{i}~{j}}, {d r_phi~{i}~{j}} ];
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }

                                Integral { [r[] * Dof{phi_rz~{h}~{i}~{j}}, {phi_rz~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                Integral { [r[] * Dof{phi_phi~{h}~{i}~{j}}, {phi_phi~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }    
                                    
                                Integral { [ OSRC_Bj[]{h,NP_OSRC,theta_branch} * r[] *
                                    Dof{d rho_rz~{h}~{i}~{j}}, {phi_rz~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                Integral { [ OSRC_Bj[]{h,NP_OSRC,theta_branch} * r[] *
                                    Dof{d rho_phi~{h}~{i}~{j}}, {phi_phi~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }    
                                    
                                Integral { [ -OSRC_Bj[]{h,NP_OSRC,theta_branch} / keps[]^2 * r[] *
                                    Dof{d phi_rz~{h}~{i}~{j}}, {d phi_rz~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                Integral { [ -OSRC_Bj[]{h,NP_OSRC,theta_branch} / keps[]^2 * r[] *
                                    Dof{d phi_phi~{h}~{i}~{j}}, {d phi_phi~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }    
                                    
                                Integral { [ -r[] * Dof{r_rz~{i}~{j}}, {phi_rz~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                Integral { [ -r[] * Dof{r_phi~{i}~{j}}, {phi_phi~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }    

                                Integral { [r[] * Dof{rho_rz~{h}~{i}~{j}} , {rho_rz~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                Integral { [r[] * Dof{rho_phi~{h}~{i}~{j}} , {rho_phi~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }    
                                    
                                Integral { [r[] * 1/keps[]^2 * Dof{phi_rz~{h}~{i}~{j}} , {d rho_rz~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                                Integral { [r[] * 1/keps[]^2 * Dof{phi_phi~{h}~{i}~{j}} , {d rho_phi~{h}~{i}~{j}} ];
                                    In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }    
                                    
                            EndFor
                        EndFor
                    EndIf*/
                }
            ElseIf (isCavity == 1) // cavity, eigenvalue problem
                // FIXME cavity not implemented yet
                /*
                Equation {
                    Integral { [nu_r[] * Dof{d e}, {d e} ];
                        In Dielectric; Jacobian Jac; Integration Int; }
                    Integral { DtDtDof[ eps_r[] * omega^2 / c[]^2 * Dof{e}, {e} ];
                        In Dielectric; Jacobian Jac; Integration Int; }
                }*/
            EndIf
        }
        EndIf
        If(ansatz == 2)
        {Name Vol_Formulation_2~{i}; Type FemEquation;
            Quantity {
                { Name e_rz~{i}; Type Local; NameOfSpace Hcurl_e_rz~{i}; }
                { Name e_phi_star~{i}; Type Local; NameOfSpace H1_e_phi_star~{i};}
                { Name e_phi~{i}; Type Local; NameOfSpace H1_e_phi~{i};}
                { Name lambda_rz~{i}; Type Local; NameOfSpace Hcurl_lambda~{i};}
                { Name lambda_phi~{i}; Type Local; NameOfSpace H1_lambda~{i};}
                
            }
            If (isCavity == 0) // waveguide, direct problem
                Equation {
                    // volume problem
                    Integral { [- eps_r[] * r[] * omega^2 / c[]^2 * Dof{e_rz~{i}}, {e_rz~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }
                    Integral { [- eps_r[] / r[] * omega^2 / c[]^2 * Dof{e_phi_star~{i}}, {e_phi_star~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }
                    //
                    Integral { [nu_r[] * r[] * Dof{d e_rz~{i}}, {d e_rz~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }
                    Integral { [nu_r[] / r[] * Dof{d e_phi_star~{i}}, {d e_phi_star~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }    
                    //
                    Integral { [m^2 / r[] * nu_r[] * Dof{e_rz~{i}}, {e_rz~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; } 
                    Integral { [m_sign * m * nu_r[] / r[] * Dof{d e_phi_star~{i}}, {e_rz~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; }     
                    Integral { [m_sign * m * nu_r[] / r[] * Dof{e_rz~{i}}, {d e_phi_star~{i}} ];
                        In Vol~{i}; Jacobian JacVol; Integration Int; } 
                    // ABC (0-th order sommerfeld)
                    Integral { [I[] * nu_r[] * r[] * k_inf * Dof{e_rz~{i}} , {e_rz~{i}} ]; 
                        In Gamma_Inf~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [I[] * nu_r[] / r[] * k_inf * Dof{e_phi_star~{i}} , {e_phi_star~{i}} ]; 
                        In Gamma_Inf~{i}; Jacobian JacSur; Integration Int; }  
                        
                    // artificial sources (g_in)
                    
                    For jj In {0:#myDomains~{i}()-1}
                        j = myDomains~{i}(jj);
                        Integral{[$ArtificialSource~{j} ?  g_in_rz~{i}~{j}[] : Vector[0,0,0], {e_rz~{i}}]; 
                            In Sigma~{i}~{j}; Jacobian JacSur; Integration Int;}
                        Integral{[($ArtificialSource~{j} ?  g_in_phi_star~{i}~{j}[] : 0), {e_phi_star~{i}}]; 
                            In Sigma~{i}~{j}; Jacobian JacSur; Integration Int;}
                    EndFor
                    
                    // excitation using lagrange multipliers
                    // in-plane
                    
                    Integral{ [Dof{lambda_rz~{i}}, {e_rz~{i}}];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int;}
                    Integral { [ 0*Dof{lambda_rz~{i}} , {lambda_rz~{i}} ]; // don't remove this
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [ Dof{e_rz~{i}} , {lambda_rz~{i}} ];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [ ($PhysicalSource ? -f_exc_rz[] 
                        : Vector[0,0,0]), {lambda_rz~{i}} ];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    
                     // out-of-plane
                    Integral{ [Dof{lambda_phi~{i}} / r[], {e_phi_star~{i}}];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int;}
                    Integral { [ 0*Dof{lambda_phi~{i}} , {lambda_phi~{i}} ]; // don't remove this
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [ Dof{e_phi_star~{i}} / r[], {lambda_phi~{i}} ];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                    Integral { [ ($PhysicalSource ? -f_exc_phi[] 
                        : Vector[0,0,0]), {lambda_phi~{i}} ];
                        In Gamma_D~{i}; Jacobian JacSur; Integration Int; }
                        
                    // transmission condition
                    If (tc_type == 0) // zeroth-order
                        For jj In {0:#myDomains~{i}()-1}
                            j = myDomains~{i}(jj);
                            Integral{[- I[] * r[] * kIBC[] * Dof{e_rz~{i}}, {e_rz~{i}}];
                                In Sigma~{i}~{j}; Jacobian JacSur; Integration Int;}
                            Integral{[- I[] / r[] * kIBC[] * Dof{e_phi_star~{i}}, {e_phi_star~{i}}];
                                In Sigma~{i}~{j}; Jacobian JacSur; Integration Int;}
                        EndFor
                    EndIf
                }
            ElseIf (isCavity == 1) // cavity, eigenvalue problem
                // FIXME cavity not implemented yet
                /*
                Equation {
                    Integral { [nu_r[] * Dof{d e}, {d e} ];
                        In Dielectric; Jacobian Jac; Integration Int; }
                    Integral { DtDtDof[ eps_r[] * omega^2 / c[]^2 * Dof{e}, {e} ];
                        In Dielectric; Jacobian Jac; Integration Int; }
                }*/
            EndIf
            
            // projection to recover e_phi
            
            Equation {
                Integral {[Dof{e_phi~{i}}, {e_phi~{i}}];
                    In Vol~{i}; Jacobian JacVol; Integration Int;}
                Integral {[- 1 / r[] * Dof{e_phi_star~{i}}, {e_phi~{i}}];
                    In Vol~{i}; Jacobian JacVol; Integration Int;}
            }  
            
        }
        EndIf
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            // surface problem - finding g^n+1 based on volume solution e^n+1
            // g^n+1 is called g_out here. Previous g^n is called g_in
            {   Name Sur_Formulation~{i}~{j}; Type FemEquation;
                Quantity{
                    { Name g_out_rz~{i}~{j}; Type Local; NameOfSpace Hcurl_g_out~{i}~{j}; }
                    If (ansatz == 1)
                        { Name g_out_phi~{i}~{j}; Type Local; NameOfSpace H1_g_out~{i}~{j}; }
                        //
                        If (tc_type == 0) // zero-order TC
                            { Name e_rz~{i}; Type Local; NameOfSpace Hcurl_e_rz~{i};}
                            { Name e_phi~{i}; Type Local; NameOfSpace H1_e_phi~{i};}
                        EndIf
                    ElseIf (ansatz == 2)
                        { Name g_out_phi_star~{i}~{j}; Type Local; NameOfSpace H1_g_out~{i}~{j}; }
                        //
                        If (tc_type == 0) // zero-order TC
                            { Name e_rz~{i}; Type Local; NameOfSpace Hcurl_e_rz~{i};}
                            { Name e_phi_star~{i}; Type Local; NameOfSpace H1_e_phi_star~{i};}
                        EndIf
                    EndIf
                    
                    /*            If (ansatz == 1)
                    If(tc_type == 2)
                        { Name r_rz~{i}~{j}; Type Local;  NameOfSpace Hcurl_r~{i}~{j};}
                        { Name r_phi~{i}~{j}; Type Local;  NameOfSpace Hcurl_r~{i}~{j};}
                        For h In {1:NP_OSRC}
                            { Name rho_rz~{h}~{i}~{j}; Type Local;  NameOfSpace Hgrad_rho~{h}~{i}~{j};}
                            { Name rho_phi~{h}~{i}~{j}; Type Local;  NameOfSpace Hgrad_rho~{h}~{i}~{j};}
                            { Name phi_rz~{h}~{i}~{j}; Type Local;  NameOfSpace Hcurl_phi~{h}~{i}~{j};}
                            { Name phi_phi~{h}~{i}~{j}; Type Local;  NameOfSpace Hcurl_phi~{h}~{i}~{j};}
                        EndFor
                    EndIf*/
                }
                Equation{
                    Integral{[Dof{g_out_rz~{i}~{j}}, {g_out_rz~{i}~{j}}]; 
                        In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                    Integral{[($PhysicalSource ? Vector[0,0,0] : g_in_rz~{i}~{j}[]), {g_out_rz~{i}~{j}} ];
                        In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                        
                    If (ansatz == 1)
                        Integral{[Dof{g_out_phi~{i}~{j}}, {g_out_phi~{i}~{j}}]; 
                            In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                        Integral{[($PhysicalSource ? Vector[0,0,0] : g_in_phi~{i}~{j}[]), {g_out_phi~{i}~{j}} ];
                            In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                        //
                        If (tc_type == 0)
                            Integral{[ -2 * I[] * r[] * kIBC[] * {e_rz~{i}}, {g_out_rz~{i}~{j}}]; 
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                            Integral{[ -2 * I[] * r[] * kIBC[] * {e_phi~{i}}, {g_out_phi~{i}~{j}}]; 
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                        EndIf
                    ElseIf (ansatz == 2)
                         Integral{[Dof{g_out_phi_star~{i}~{j}}, {g_out_phi_star~{i}~{j}}]; 
                            In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                         Integral{[($PhysicalSource ? Vector[0,0,0] : g_in_phi_star~{i}~{j}[]), {g_out_phi_star~{i}~{j}} ];
                            In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                        // 
                        If (tc_type == 0)
                            Integral{[ -2 * I[] * r[] * kIBC[] * {e_rz~{i}}, {g_out_rz~{i}~{j}}]; 
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                            Integral{[ -2 * I[] / r[] * kIBC[] * {e_phi_star~{i}}, {g_out_phi_star~{i}~{j}}]; 
                                In Sigma~{i}~{j}; Integration Int; Jacobian JacSur;}
                        EndIf
                    EndIf

                    /*
                    If(tc_type == 2)
                        Integral { [ -2 * I[] * r[] * kIBC[] * {r_rz~{i}~{j}} , {g_out_rz~{i}~{j}} ];
                        In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                        Integral { [ -2 * I[] * r[] * kIBC[] * {r_phi~{i}~{j}} , {g_out_phi~{i}~{j}} ];
                        In Sigma~{i}~{j}; Integration Int; Jacobian JacSur; }
                    EndIf*/
                }
            }
        EndFor
    EndFor
}

PostProcessing {
  For ii In {0:#myDomains()-1}
    i = myDomains(ii);
    { Name e_post_pro~{i} ; NameOfFormulation Vol_Formulation~{ansatz}~{i};
        Quantity {
            { Name e_rz~{i} ; Value { Local{[{e_rz~{i}}] ; In Vol~{i}; Jacobian JacVol; } } }
            { Name e_phi~{i}; Value { Local{[{e_phi~{i}}]; In Vol~{i}; Jacobian JacVol; } } }
            If (ansatz == 2)
            { Name e_phi_star~{i}; Value { Local{[{e_phi_star~{i}}]; In Vol~{i}; Jacobian JacVol; } } }
            EndIf
        }
    }
    For jj In {0:#myDomains~{i}()-1}
        j = myDomains~{i}(jj);
        { Name Sur~{i}~{j}; NameOfFormulation Sur_Formulation~{i}~{j}; 
            Quantity {
                { Name g_out_rz~{i}~{j}; 
                Value { Local {[{g_out_rz~{i}~{j}}]; In Sigma~{i}~{j}; Jacobian JacSur;}}} 
                If (ansatz == 1)
                    { Name g_out_phi~{i}~{j}; 
                    Value { Local {[{g_out_phi~{i}~{j}}]; In Sigma~{i}~{j}; Jacobian JacSur;}}}
                ElseIf (ansatz == 2)
                    { Name g_out_phi_star~{i}~{j}; 
                    Value { Local {[{g_out_phi_star~{i}~{j}}]; In Sigma~{i}~{j}; Jacobian JacSur;}}}
                EndIf
            }
        }
    EndFor
    { Name rel_err_calc~{i}; NameOfFormulation Vol_Formulation~{ansatz}~{i};
        Quantity {
            { Name used_iterations; Value{ Term {[$KSPIterations]; In Dummy; Type Global;  }}}
            { Name squared_norm_phi~{i}; Value{
                Integral{ Type Global;
                    [SquNorm[f_ana_phi[]]];
                    In Vol~{i}; Jacobian JacVol; Integration Int;}}
            }
            { Name squared_err_phi~{i}; Value{
            Integral{ Type Global;
                    [SquNorm[f_ana_phi[] - {e_phi~{i}}]];
                    In Vol~{i}; Jacobian JacVol; Integration Int;}}
            }
            { Name squared_err_rz~{i}; Value{
            Integral{ Type Global;
                    [SquNorm[f_ana_rz[] - {e_rz~{i}}]]; 
                    In Vol~{i}; Jacobian JacVol; Integration Int;}}
            }
            { Name squared_norm_rz~{i}; Value{
            Integral{ Type Global; 
                    [SquNorm[f_ana_rz[]]];
                    In Vol~{i}; Jacobian JacVol; Integration Int;}}
            }
        }
    }
  EndFor
}

PostOperation {
  For ii In {0:#myDomains()-1}
    i = myDomains(ii);
        { Name ddm~{i}; NameOfPostProcessing e_post_pro~{i}; 
            Operation{
                Print [ e_rz~{i}, OnElementsOf Vol~{i}, 
                    File StrCat[out_dir, "/", Sprintf("e_rz_%g.pos", i)]];
                Print [ e_phi~{i}, OnElementsOf Vol~{i}, 
                    File StrCat[out_dir, "/", Sprintf("e_phi_%g.pos", i)]];
                If (ansatz == 2)
                //Print [ e_phi_star~{i}, OnElementsOf Vol~{i}, 
                //    File StrCat[out_dir, "/", Sprintf("e_phi_%g.pos", i)]];
                EndIf
            }
        } 
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            { Name g_out~{i}~{j}; NameOfPostProcessing Sur~{i}~{j}; 
                Operation{
                    Print[ g_out_rz~{i}~{j}, OnElementsOf Sigma~{i}~{j},
                        StoreInField tag_g_rz~{i}~{j}, AtGaussPoints IntPointsLine 
                        //,File StrCat[out_dir, "/", Sprintf("gg_rz_%g_%g.pos", i, j)]
                         ];
                    If (ansatz == 1)
                    Print[ g_out_phi~{i}~{j}, OnElementsOf Sigma~{i}~{j},
                        StoreInField tag_g_phi~{i}~{j}, AtGaussPoints IntPointsLine
                        //,File StrCat[out_dir, "/", Sprintf("gg_phi_%g_%g.pos", i, j)]
                         ];
                    ElseIf (ansatz == 2)
                    Print[ g_out_phi_star~{i}~{j}, OnElementsOf Sigma~{i}~{j},
                        StoreInField tag_g_phi~{i}~{j}, AtGaussPoints IntPointsLine
                        //,File StrCat[out_dir, "/", Sprintf("gg_phi_star_%g_%g.pos", i, j)]
                         ];
                    EndIf
                }
            }
        EndFor
        { Name rel_err_calc~{i}; NameOfPostProcessing rel_err_calc~{i};
            Operation{
                // print integral over error norm
                Echo[Sprintf["Domain %g: Integral of squared error, inplane:", i], Format Table, File > convergence_output_filename];
                Print[squared_err_rz~{i}, OnGlobal, File > convergence_output_filename, Format Table];
                Echo[Sprintf["Domain %g: Integral of squared norm of analytical solution, inplane:", i], Format Table, File > convergence_output_filename];
                Print[squared_norm_rz~{i}, OnGlobal, File > convergence_output_filename, Format Table];
                Echo[Sprintf["Domain %g: Integral of squared error, outofplane:", i], Format Table, File > convergence_output_filename];
                Print[squared_err_phi~{i}, OnGlobal, File > convergence_output_filename, Format Table];
                Echo[Sprintf["Domain %g: Integral of squared norm of analytical solution, outofplane:", i], Format Table, File > convergence_output_filename];
                Print[squared_norm_phi~{i}, OnGlobal, File > convergence_output_filename, Format Table];
                Echo["", Format Table, File > convergence_output_filename];
            }
        }
  EndFor
  { Name print_run_info; NameOfPostProcessing rel_err_calc_0;
        Operation{
            
            Echo["------------------------------------------------------------------------------", Format Table, File > convergence_output_filename];  
            Echo[
                Sprintf[
                    StrCat[
                        StrChoice[exctype==1, "TE", "TM"],
                        "_%g%g mode at %g elements per wavelength"
                    ], 
                    m, n, no_of_ele_per_lambda], 
                Format Table, File > convergence_output_filename];  
            Echo[Sprintf["L=%g  R=%g    omega=%g    omega_c=%g", L, R, omega, omega_c], Format Table, File > convergence_output_filename];
            Echo[Sprintf["Number of subdomains: %g", no_of_subdomains],  File > convergence_output_filename];
            Echo[StrCat[
                    "Ansatz: ",
                    StrChoice[ansatz==1, "none", "e-phi-star"],
                    "   ",
                    "Transmission condition:",
                    StrChoice[tc_type==0, " Order zero", "Pade"]
                ]
                , Format Table, File > convergence_output_filename];
            Echo[Sprintf[
                    "Iterative solver limits: Max. It. %g   Tolerance %g", max_iterations, tolerance
                ],
                Format Table, File > convergence_output_filename];  
            /*Echo[Sprintf["Iterative solver actual iterations "]
                , Format Table, File > convergence_output_filename];
            Print[used_iterations, OnGlobal, File > convergence_output_filename, Format Table];*/
            Echo[StrCat[
                    StrChoice[highOrder==1, "High order ", "No high order "],
                    "basis functions were used."
                ], Format Table, File > convergence_output_filename];
            Echo["------------------------------------------------------------------------------", Format Table, File > convergence_output_filename];
        }
  }
}
