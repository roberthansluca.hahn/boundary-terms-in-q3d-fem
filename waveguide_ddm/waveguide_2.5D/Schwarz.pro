DefineConstant[
    // Macro to combine all subdomain views
    COMBINE = {Str[ "For i In {no_of_subdomains-1:0:-1}",
                    "For j In {0:PostProcessing.NbViews-1}",
                    "View[j].Name=StrReplace(View[j].Name, Sprintf('_%g', i), '');",
                    "EndFor",
                    "EndFor",
                    "Combine ElementsByViewName;"],
                    Name "Macros/Combine subdomain results", AutoCheck 0, Macro "GmshParseString"},
    // Macro to convert visible views from harmonic to time domain
    H2T = {Str[ "For j In {0:PostProcessing.NbViews-1}",
                "If(View[j].Visible)",
                "Plugin(HarmonicToTime).View = j;",
                "Plugin(HarmonicToTime).TimeSign = 1;",
                "Plugin(HarmonicToTime).Run;",
                "EndIf",
                "EndFor"],
                Name "Macros/Convert visible views to time-domain", AutoCheck 0, Macro "GmshParseString"}
];

Macro Init
    // reset all flags
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        GenerateVolFlag~{i} = 0;
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            GenerateSurFlag~{i}~{j} = 0;
        EndFor
    EndFor
Return
//
Macro PrintInfo
    Printf[StrCat["Starting Maxwell DDM with %g subdomains / %g processes"], no_of_subdomains, MPI_Size];
    If(tc_type == 0)
      Printf[StrCat["Using 0-th order (Silver-Muller) transmission conditions"]];
    EndIf
    If(tc_type == 2)
      Printf["Using %g-th order Pade (OSRC) transmission conditions", NP_OSRC];
    EndIf
    Printf["Relative iterative solver tolerance = %g", tolerance];
    // preallocation increase needed for maxwell problems
    SetGlobalSolverOptions["-petsc_prealloc 200"];
Return
//
Macro SolveVolumeProblem
    SetCommSelf;
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        Printf[Sprintf["Solving volume problem for subdomain i=%g", i]];
        If(GenerateVolFlag~{i} == 1)
            // matrix for this volume has already been treated. Regenerate right hand side.
            Test[$PhysicalSource == 1 ]
            // perform this operation if physical sources are active
            {
                GenerateRHSGroup[Vol_Resolution~{i}, Region[{Sigma~{i}, Gamma_D~{i}}]];
            }
            // perform this operation if physical sources are inactive
            {
                GenerateRHSGroup[Vol_Resolution~{i}, Region[{Sigma~{i}, Gamma_D~{i}}] ];
            }
            SolveAgain[Vol_Resolution~{i}];
        ElseIf (GenerateVolFlag~{i} == 0)
            // solve for the first time
            Generate[Vol_Resolution~{i}]; 
            Solve[Vol_Resolution~{i}];
            GenerateVolFlag~{i} = 1;
        EndIf
    EndFor
    SetCommWorld;
Return
//
Macro SolveSurfaceProblem
    SetCommSelf;
    // compute new g_in
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            If (NbrRegions[Sigma~{i}~{j}] > 0)
                Printf[Sprintf["Solving Surface problem for i=%g, j=%g", i, j]];
                If (GenerateSurFlag~{i}~{j} == 1)
                    // matrix has been treated, only generate RHS
                    GenerateRHSGroup[Sur_Resolution~{i}~{j}, Region[{Sigma~{i}~{j}, BndSigmaInf~{i}~{j}}]];
                    SolveAgain[Sur_Resolution~{i}~{j}];
                ElseIf (GenerateSurFlag~{i}~{j} == 0)
                    // solve for the first time
                    Generate[Sur_Resolution~{i}~{j}];
                    Solve[Sur_Resolution~{i}~{j}];
                    GenerateSurFlag~{i}~{j} = 1;
                EndIf
            EndIf
        EndFor
    EndFor
    SetCommWorld;
Return
//
Macro UpdateSurfaceFields
    SetCommSelf;
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            PostOperation[g_out~{i}~{j}];
        EndFor
    EndFor
    SetCommWorld;
Return
//
Macro DisableArtificialSources
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            Evaluate[$ArtificialSource~{j} = 0];
        EndFor
    EndFor
Return
//
Macro EnableArtificialSources
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        For jj In {0:#myDomains~{i}()-1}
            j = myDomains~{i}(jj);
            Evaluate[$ArtificialSource~{j} = 1];
        EndFor
    EndFor
Return
//
Macro DisablePhysicalSources
    Evaluate[$PhysicalSource = 0]; 
Return
//
Macro EnablePhysicalSources
    Evaluate[$PhysicalSource = 1]; 
Return
//
Macro SaveVolumeSolutions
    SetCommSelf;
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        PostOperation[ddm~{i}];
    EndFor
    SetCommWorld;
Return
//
Macro PrintErrorInfo // calculate and print squared errors and norms
    SetCommSelf;
    // print info for current run to log file
    PostOperation[print_run_info];
    For ii In {0:#myDomains()-1}
        i = myDomains(ii);
        PostOperation[rel_err_calc~{i}];
    EndFor
    SetCommWorld;
Return
//
Resolution {
    { Name schwarz_ddm;
        System {
            For ii In {0:#myDomains()-1}
                i = myDomains(ii);
                { Name Vol_Resolution~{i}; NameOfFormulation Vol_Formulation~{ansatz}~{i}; Type Complex; 
                //NameOfMesh Sprintf[StrCat[mesh_dir, "/", msh_base_name, "_%g.msh"], i];
                }
                For jj In {0:#myDomains~{i}()-1}
                    j = myDomains~{i}(jj);
                    { Name Sur_Resolution~{i}~{j}; NameOfFormulation Sur_Formulation~{i}~{j}; Type Complex; 
                        //NameOfMesh Sprintf[StrCat[mesh_dir, "/", msh_base_name, "_%g.msh"], i]; 
                    }
                EndFor
            EndFor
        }
        Operation{
            // Reset flags
            Call Init;
            
            // Print general info
            Call PrintInfo;
            
            // solve for physical excitation. 
            // - enable physical, disable artificial sources
            // - Solve volume problem
            // - Solve Surface problem and update Surface fields
            Call EnablePhysicalSources;
            Call DisableArtificialSources;
            Call SolveVolumeProblem;
            Call SolveSurfaceProblem;
            Call UpdateSurfaceFields;
            
            // Call iterative solver to solve for artificial sources
            Call DisablePhysicalSources;
            Call EnableArtificialSources;
            
            // timing
            Evaluate[ $tt1 = GetWallClockTime[], $tt1c = GetCpuTime[] ];
            
            If (no_of_subdomains > 1)
                Printf["Starting IterativeSolver"];
                IterativeLinearSolver["I-A", solver, tolerance, max_iterations, restart, 
                                    {ListOfFields()}, {ListOfConnectedFields()}, {}]
                {
                    // timing
                    Evaluate[ $t1 = GetWallClockTime[], $t1c = GetCpuTime[] ];
                    
                    // in each iteration, solve volume problem, solve surface problem, update surface fields
                    Call SolveVolumeProblem;
                    Call SolveSurfaceProblem;
                    Call UpdateSurfaceFields;
                    
                    // multithreading
                    Barrier;
                    
                    // timing
                    Evaluate[ $t2 = GetWallClockTime[], $t2c = GetCpuTime[] ];
                    Print[{$t2-$t1, $t2c-$t1c}, Format "WALL Schwarz iteration = %gs ; CPU = %gs"];
                    
                    // why is this not working ?!
                    /*
                    SetNumberRunTime
                        [
                            GetNumberRunTime
                                []
                                {$KSPIterations}
                        ] 
                        {used_iterations};*/
                    //Evaluate[$used_iterations = $KSPIteration];
                    //Print[{$KSPIteration}];
                }
                {
                    // no preconditioner
                }
            EndIf
            // timing
            Evaluate[ $tt2 = GetWallClockTime[], $tt2c = GetCpuTime[] ];
            Print[{$tt2-$tt1, $tt2c-$tt1c}, Format "WALL total DDM solver = %gs ; CPU = %gs"];
            Print[{$KSPIterations}];
            // Enable both physical and artificial sources to construct solution. 
            Call EnablePhysicalSources;
            Call EnableArtificialSources;
            Call SolveVolumeProblem;
            Call SaveVolumeSolutions;
            If (execute_error_calc)
                //Evaluate[Printf[{$KSPIterations}]];
                Call PrintErrorInfo;
            EndIf
        }
    }
} 
