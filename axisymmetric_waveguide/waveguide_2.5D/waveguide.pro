/*
Problem description of an axisymmetric waveguide
*/

Include "../shared_pro.dat";
Include "../shared_functions.dat";

Group {
    //Physical regions
    Dielectric = Region[{1004}];
    Port_1 = Region[{1000}];
    Bnd_PEC = Region[{1001}];
    Bnd_Truncated = Region[{1002}];
    Bnd_Symm_Axis = Region[{1003}];
    
    Print_Point = Region[{10000}];
    
    SurAll = Region[{Bnd_PEC, Bnd_Truncated, Bnd_Symm_Axis, Port_1}];
    VolAll = Region[{Dielectric}];
    TotAll = Region[{VolAll, SurAll}];
    
    PortsAll = Region[{Port_1, Bnd_Truncated}];
}

DefineConstant[
    ANSATZ = {(m>0?2:1), Name "Input/05Q3D/Ansatz",  Choices{1 = "none", 2 = "e_phi_star"}}, // ANSATZ to use. none is only suitable for m == 0
    //phi = {0, Min 0, Max 2*Pi, Name "Phi"}, // angle, at which the slice is positioned. Irrelevant for formulation and excitation, as we deal with Fourier coefficients here. Might be interesting to reconstruct a solution comparable to full 3D
    convergence_output_filename = {"convergence_log.log"},
    eigenvalues_output_filename = {"EigenValuesReal.txt"}
];

C = 1;

// constant  strings for convergence logs
If (EXC_TYPE==1) mode_type_str = "TE";
ElseIf (EXC_TYPE==2) mode_type_str = "TM";
EndIf
If (ANSATZ==1) ANSATZ_str = "No Ansatz";
ElseIf (ANSATZ==2) ANSATZ_str = "e_phi star Ansatz";
EndIf
If (CAVITY==1) cavity_str = "cavity";
ElseIf (CAVITY==0) cavity_str = "non-cavity";
EndIf
If (COMPLETE_BASIS == 1) order_str = "high order";
ElseIf (COMPLETE_BASIS == 0) order_str = "regular";
EndIf

If (M_SIGN == -1) mode_sign_str = "-m";
ElseIf (M_SIGN == 1) mode_sign_str = "+m";
EndIf

Function{ 
    //unit vectors of cyl. coordinates
    unitPhi[] = Vector[0, 0, -1];
    unitR[] = Vector[1, 0, 0];
    unitZ[] = Vector[0, 1, 0];
    
    //Radial coordinate
    r[] = X[];
    
	// propagation direction
    z[] = Y[];
    
    // complex unit
    I[] = Complex[0., 1.];
    
	//Excitation
	If (EXC_TYPE==1) //TE
		// analytical solution from pozar's book at z=0. In this formulation, A and B can not both be non-zero at the same time.
		// A and B are set based on the mode_sign choice (+m / -m)
		f_exc_inplane[Port_1] = (-I[] * omega * mu[] * m /k_r^2 / r[]) * (A-B) * Jn[m, k_r * r[]]* unitR[]; 
		f_exc_outofplane[Port_1] = (I[] * omega * mu[] / k_r) * (A+B) * dJn[m, k_r * r[]];
	ElseIf (EXC_TYPE==2) //TM
		// analytical solution from pozar's book at z=0. In this formulation, A and B can not both be non-zero at the same time.
		// A and B are set based on the mode_sign choice (+m / -m)
		f_exc_inplane[Port_1] = (-I[] * k_z / k_r) * (B+A) * dJn[m, k_r * r[]] * unitR[] 
							+ (B+A) * Jn[m, k_r * r[]] * unitZ[]; 
		f_exc_outofplane[Port_1] = (I[] * k_z * m / k_r^2 / r[]) * (B-A) * Jn[m, k_r * r[]];
	EndIf

    //analytical solution for error calculation
    If (CAVITY == 1)
		If (EXC_TYPE==1) //TE
			// analytical solution from pozar's book
			f_ana_inplane[] =  I[] * omega * m / k_r^2 * C * Jn[m, k_r * r[]] * Sin[p * Pi * z[] / L] * unitR[];
			f_ana_outofplane[] = I[] * omega / k_r * C * dJn[m, k_r * r[]] * Sin[p * Pi * z[] / L];
		ElseIf (EXC_TYPE==2) //TM
			// analytical solution from pozar's book
			f_ana_inplane[] = C * Jn[m, k_r * r[]] * Cos[p * Pi * z[] / L] * unitZ[] - p * Pi / L / k_r * C * dJn[m, k_r * r[]] * Sin[p * Pi * z[] / L] * unitR[];
			f_ana_outofplane[] = -p * Pi / L * m / k_r^2 * C * Jn[m, k_r * r[]] * Sin[p * Pi * z[] / L];
		EndIf
	ElseIf(CAVITY == 0)
		If (EXC_TYPE==1) //TE
			// analytical solution from pozar's book
			f_ana_inplane[] = - I[] * omega * mu[] * m / (k_r^2 * r[]) * (A - B) * Jn[m, k_r * r[]] * Exp[-I[] * k_z * z[]] * unitR[];
			f_ana_outofplane[] = I[] * omega * mu[] / k_r * (A + B) * dJn[m, k_r * r[]] * Exp[-I[] * k_z * z[]];
		ElseIf (EXC_TYPE==2) //TM
			// analytical solution from pozar's book
			f_ana_inplane[] = - I[] * k_z / k_r * (A + B) * dJn[m, k_r * r[]] * Exp[-I[] * k_z * z[]] * unitR[]
							+ (A + B)  * Jn[m, k_r * r[]] * Exp[-I[] * k_z * z[]] * unitZ[];
			f_ana_outofplane[] = - I[] * k_z * m / (k_r^2 * r[]) * (A - B) * Jn[m, k_r * r[]] * Exp[-I[] * k_z * z[]];
		EndIf
	EndIf
}

Constraint{
    {Name e_Constraint_PEC; 
		// Dirichlet Boundary condition on the mantle of the waveguide. Material assumed to be PEC.
        Case {
            {Region Bnd_PEC ; Value 0.;}
            If (CAVITY == 1)
            {Region Region [{Port_1, Bnd_Truncated}]; Value 0.;}
            EndIf
        }    
    }
    {Name e_phi_star_symm;
		// Dirichlet condition on the symmetry axis. Needed for the e_phi_star ANSATZ as outlined in Erik's thesis, p. 31
        Case {
            {Region Bnd_Symm_Axis; Value 0.;}
        }
    }
    {Name e_rz_symm;
		// Dirichlet condition on the symmetry axis. e_z = 0 needed for |m| > 0 as outlined in Erik's thesis, p. 32
		// We have no control over e_z, only over e_rz. 'Value 0.' constrains the trace, not the quantity itself, so this works as intended. 
        Case {
            {Region Bnd_Symm_Axis; Value 0.;}
        }
    }
    If (CAVITY == 0)
        { Name e_Constraint_Excitation_inplane;
            // Constrain e_inplane to match the excitation f_exc_inplane on Port_1. This requires a separate Formulation and Resolution.
            Case {
                { Region Port_1 ; Type AssignFromResolution; NameOfResolution excitation_inplane ; }
            }
        }
        { Name e_Constraint_Excitation_outofplane;
            // constrain e_outofplane to match the excitation f_exc_outofplane on Port_1. This requires a separate Formulation and Resolution. 
            Case {
                { Region Port_1 ; Type AssignFromResolution; NameOfResolution excitation_outofplane ; }
            }
        }
	EndIf
}

FunctionSpace {
    {Name Hcurl_e_2D; Type Form1;
		// function space for e_inplane
        BasisFunction{
            {   Name basis_inplane; NameOfCoef inplane_coeff; Function BF_Edge;
                Support TotAll; Entity EdgesOf[All];   }
            If (COMPLETE_BASIS == 1)
                {   Name basis_inplane2; NameOfCoef inplane_coeff2; Function BF_Edge_2E;
                    Support TotAll; Entity EdgesOf[All];    }
            EndIf
        }
        Constraint {
            {   NameOfCoef inplane_coeff; EntityType EdgesOf;
                NameOfConstraint e_Constraint_PEC;
            }
            {
                NameOfCoef inplane_coeff; EntityType EdgesOf;
                NameOfConstraint e_Constraint_Excitation_inplane;
            }
            If (m != 0) // the condition on the symmetry axis only applies for |m| > 0
                {
                    NameOfCoef inplane_coeff; EntityType EdgesOf;
                    NameOfConstraint e_rz_symm;
                }
            EndIf
            If (COMPLETE_BASIS == 1)
                {   NameOfCoef inplane_coeff2; EntityType EdgesOf;
                    NameOfConstraint e_Constraint_PEC;
                }
                {
                    NameOfCoef inplane_coeff2; EntityType EdgesOf;
                    NameOfConstraint e_Constraint_Excitation_inplane;
                }
                If (m != 0) // the condition on the symmetry axis only applies for |m| > 0
                {
                    NameOfCoef inplane_coeff2; EntityType EdgesOf;
                    NameOfConstraint e_rz_symm;
                }
                EndIf
            EndIf
        }
    }
    
    {Name H1_e_phi; Type Form0; 
		// function space for e_outofplane
        BasisFunction{
            {   Name basis_outofplane; NameOfCoef outofplane_coeff; Function BF_Node;
                Support TotAll; Entity NodesOf[All];   }            
            If (COMPLETE_BASIS == 1)
                {   Name basis_outofplane2; NameOfCoef outofplane_coeff2; Function BF_Node_2E;
                    Support TotAll; Entity EdgesOf[All];    }
            EndIf
        }
        Constraint {
			// if the e_phi_star ANSATZ is chosen, e_phi is not used in the problem formulation. After the problem is solved for e_phi_star, e_phi is recovered through an additional projection. e_phi should therefore not be constrained in that case.
            If(ANSATZ==1) // this constraint is only needed if no special ANSATZ is chosen
                {   NameOfCoef outofplane_coeff; EntityType NodesOf;
                    NameOfConstraint e_Constraint_PEC; 
                }
                {
                    NameOfCoef outofplane_coeff; EntityType NodesOf;
                    NameOfConstraint e_Constraint_Excitation_outofplane;
                }
                If (COMPLETE_BASIS == 1)
                    {   NameOfCoef outofplane_coeff2; EntityType EdgesOf;
                        NameOfConstraint e_Constraint_PEC; 
                    }
                    {
                        NameOfCoef outofplane_coeff2; EntityType EdgesOf;
                        NameOfConstraint e_Constraint_Excitation_outofplane;
                    }
                EndIf
            EndIf
            { NameOfCoef outofplane_coeff; EntityType NodesOf;
                NameOfConstraint e_phi_star_symm;
            }
            If (COMPLETE_BASIS == 1)
                { NameOfCoef outofplane_coeff2; EntityType EdgesOf;
                    NameOfConstraint e_phi_star_symm;
                }
            EndIf
        }
    }
    
    {Name H1_e_phi_star; Type Form0; // this function space is only needed for the e_phi_star ANSATZ
		// function space for e_phi_star
        BasisFunction{
            {   Name basis_star; NameOfCoef star_coeff; Function BF_Node;
                Support TotAll; Entity NodesOf[All];   }
            If (COMPLETE_BASIS == 1)
                {   Name basis_star2; NameOfCoef star_coeff2; Function BF_Node_2E;
                    Support TotAll; Entity EdgesOf[All];   }
            EndIf
        }
        Constraint {
            If (ANSATZ == 2) // for the sake of completeness, these constraints are - like the whole function space - only needed for the e_phi_star ANSATZ.
                {   NameOfCoef star_coeff; EntityType NodesOf;
                    NameOfConstraint e_Constraint_PEC; 
                }
                {   NameOfCoef star_coeff; EntityType NodesOf;
                    NameOfConstraint e_phi_star_symm; 
                }
                // e_Constraint_Excitation_outofplane uses the Resolution excitation_outofplane which in turn chooses the formulation based on the ANSATZ used. 
                // so e_phi_star can use the same excitation Constraint as e_outofplane
                {   NameOfCoef star_coeff; EntityType NodesOf;
                    NameOfConstraint e_Constraint_Excitation_outofplane;
                }
                If (COMPLETE_BASIS == 1)
                    {   NameOfCoef star_coeff2; EntityType EdgesOf;
                        NameOfConstraint e_Constraint_PEC; 
                    }
                    {   NameOfCoef star_coeff2; EntityType EdgesOf;
                        NameOfConstraint e_phi_star_symm; 
                    }
                    // e_Constraint_Excitation_outofplane uses the Resolution excitation_outofplane which in turn chooses the formulation based on the ANSATZ used. 
                    // so e_phi_star can use the same excitation Constraint as e_outofplane
                    {   NameOfCoef star_coeff2; EntityType EdgesOf;
                        NameOfConstraint e_Constraint_Excitation_outofplane;
                    }
                EndIf
            EndIf
        }
    }
}

Jacobian { // transformation to ref. element. vol means the regular transformation
    {   Name Jac ;
        Case{   {Region VolAll;
                Jacobian Vol ;}
                {Region SurAll; 
                Jacobian Sur;}
                { Region Print_Point ; Jacobian Vol ; } 
        }
    }
}

Integration {
    {   Name Int;
        Case{   {   Type Gauss;
                    Case { 
                        {GeoElement Triangle;   NumberOfPoints 16;}
                        {GeoElement Line ;      NumberOfPoints 8;}
                    }
                }
        }
    }
}

Formulation {
    // excitation
	// this finds the best Dof{e}, such that {e} is as close as possible to f_exc on Port_1
	If (CAVITY == 0)
        // inplane
        {   Name excitation_dirichlet_inplane;
            Quantity {
                { Name e_inplane; Type Local; NameOfSpace Hcurl_e_2D; }
            }
            Equation {
                // weak formulation of e_inplane = f_exc_inplane
                Galerkin { [ Dof{e_inplane} , {e_inplane} ];
                In Port_1; Integration Int; Jacobian Jac;  }
                Galerkin { [ -f_exc_inplane[] , {e_inplane} ];
                In Port_1; Integration Int; Jacobian Jac;  }
            }
        }
        // outofplane
        {   Name excitation_dirichlet_outofplane_1;
            Quantity {
                { Name e_outofplane; Type Local; NameOfSpace H1_e_phi; }
            } 
            Equation {
                // weak formulation of e_outofplane = f_exc_outofplane
                Galerkin { [ Dof{e_outofplane} , {e_outofplane} ];
                In Port_1; Integration Int; Jacobian Jac;  }
                Galerkin { [ -f_exc_outofplane[] , {e_outofplane} ];
                In Port_1; Integration Int; Jacobian Jac;  }
            }
        }
        // e_phi_star
        {   Name excitation_dirichlet_outofplane_2;
            Quantity {
                { Name e_phi_star; Type Local; NameOfSpace H1_e_phi_star; }
            }
            Equation {
                // weak formulation of e_phi_star (= r * e_phi) = r * f_exc_outofplane
                Galerkin { [ Dof{e_phi_star} , {e_phi_star} ];
                In Port_1; Integration Int; Jacobian Jac;  }
                Galerkin { [ - r[] * f_exc_outofplane[] , {e_phi_star} ];
                In Port_1; Integration Int; Jacobian Jac;  }
            }
        }
    EndIf
    // Problem formulation. Note that dOmega = r dr dz for cylindrical coordinates.
    // This integrates over the 2D slice, so dr dz is implied. r needs to be included whereever dOmega occurs
    
    // no ANSATZ
    // expanded version of 4.19 (p. 18) in Erik's thesis. 
    // GetDP doesn't like anything other than quantities in {} on the right-hand-side of an integral, so all r-unitvectors that should be on the RHS (from grad(r))
    // were moved to the left side of the integral. This is incorrect, but makes no difference for decoupled modes such as m=0, where this ANSATZ is used. 
    // All m were multiplied with M_SIGN, which is either -1 or +1 and represents the sign of the mode we're interested in. This should be irrelevant here, as this ANSATZ is only used for m=0.
    {   Name wave_formulation_1; Type FemEquation;
        Quantity {
            { Name e_inplane ; Type Local; NameOfSpace Hcurl_e_2D;}
            { Name e_outofplane ; Type Local; NameOfSpace H1_e_phi; }
        }
        If (CAVITY == 0)
            Equation {
                Integral { [-omega^2 / c0^2 * eps_r[] * r[] * Dof{e_inplane} , {e_inplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [-omega^2 / c0^2 * eps_r[] * r[] * Dof{e_outofplane} , {e_outofplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [nu_r[] * r[] * Dof{d e_inplane} , {d e_inplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [m^2 / r[] * nu_r[] * Dof{e_inplane} , {e_inplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                // grad_rz (r e_phi) = grad_rz(r) e_phi + r grad_rz(e_phi)
                Integral { [M_SIGN * m / r[] * nu_r[] * unitR[] * Dof{e_outofplane}, {e_inplane} ];  
                    In Dielectric; Jacobian Jac; Integration Int; } 
                Integral { [M_SIGN * m * nu_r[] * Dof{d e_outofplane}, {e_inplane} ];  
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [M_SIGN * m / r[] * nu_r[] * Dof{e_inplane} * unitR[], {e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; } 
                Integral { [M_SIGN * m * nu_r[] * Dof{e_inplane}, {d e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; } 
                // grad_rz (r e_phi) = grad_rz(r) e_phi + r grad_rz(e_phi)
                Integral { [1 / r[] * nu_r[] * Dof{e_outofplane}, {e_outofplane} ]; //omitted unitR * unitR as it is 1 anyway
                    In Dielectric; Jacobian Jac; Integration Int; }
                Integral { [nu_r[] * Dof{e_outofplane} * unitR[] , {d e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                Integral { [nu_r[] * Dof{d e_outofplane} * unitR[], {e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                Integral { [r[] * nu_r[] * Dof{d e_outofplane}, {d e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                // Boundary 
                // Include Sommerfeld boundary terms only for a non-cavity problem. 
                // For a cavity problem (homogeneous Neumann), the boundary terms can simply be omitted. 
                If (CAVITY == 0)
                Integral { [ I[] * r[] * nu_r[] * k_inf * Dof{e_inplane}, {e_inplane}];
                    In Bnd_Truncated; Jacobian Jac; Integration Int;}
                Integral { [ I[] * r[] * nu_r[] * k_inf * Dof{e_outofplane}, {e_outofplane}];
                    In Bnd_Truncated; Jacobian Jac; Integration Int;}
                EndIf 
            }
        ElseIf (CAVITY == 1)
            Equation {
                Integral { DtDtDof[1 / c0^2 * eps_r[] * r[] * Dof{e_inplane} , {e_inplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { DtDtDof[1 / c0^2 * eps_r[] * r[] * Dof{e_outofplane} , {e_outofplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [nu_r[] * r[] * Dof{d e_inplane} , {d e_inplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [m^2 / r[] * nu_r[] * Dof{e_inplane} , {e_inplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                // grad_rz (r e_phi) = grad_rz(r) e_phi + r grad_rz(e_phi)
                Integral { [M_SIGN * m / r[] * nu_r[] * unitR[] * Dof{e_outofplane}, {e_inplane} ];  
                    In Dielectric; Jacobian Jac; Integration Int; } 
                Integral { [M_SIGN * m * nu_r[] * Dof{d e_outofplane}, {e_inplane} ];  
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [M_SIGN * m / r[] * nu_r[] * Dof{e_inplane} * unitR[], {e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; } 
                Integral { [M_SIGN * m * nu_r[] * Dof{e_inplane}, {d e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; } 
                // grad_rz (r e_phi) = grad_rz(r) e_phi + r grad_rz(e_phi)
                Integral { [1 / r[] * nu_r[] * Dof{e_outofplane}, {e_outofplane} ]; //omitted unitR * unitR as it is 1 anyway
                    In Dielectric; Jacobian Jac; Integration Int; }
                Integral { [nu_r[] * Dof{e_outofplane} * unitR[] , {d e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                Integral { [nu_r[] * Dof{d e_outofplane} * unitR[], {e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                Integral { [r[] * nu_r[] * Dof{d e_outofplane}, {d e_outofplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
            }
        EndIf
    }
    
    // e_phi_star ANSATZ
    // Eq. 4.19 (p.18) in Erik's thesis. 
    // All m were multiplied with M_SIGN, which is either -1 or +1 and represents the sign of the mode we're interested in.
    {   Name wave_formulation_2; Type FemEquation;
        Quantity {
            { Name e_inplane ; Type Local; NameOfSpace Hcurl_e_2D;}
            { Name e_phi_star; Type Local; NameOfSpace H1_e_phi_star;}
            { Name e_outofplane; Type Local; NameOfSpace H1_e_phi;}
        }
        If (CAVITY == 0)
            Equation {
                Integral { [-omega^2 / c0^2 * eps_r[] * r[] * Dof{e_inplane} , {e_inplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [-omega^2 / c0^2 * eps_r[] / r[] * Dof{e_phi_star} , {e_phi_star} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [nu_r[] * r[] * Dof{d e_inplane} , {d e_inplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [m^2 / r[] * nu_r[] * Dof{e_inplane} , {e_inplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                // 
                Integral { [M_SIGN * m / r[] * nu_r[] * Dof{d e_phi_star}, {e_inplane} ];  
                    In Dielectric; Jacobian Jac; Integration Int; } 
                Integral { [M_SIGN * m / r[] * nu_r[] * Dof{e_inplane}, {d e_phi_star} ];  
                    In Dielectric; Jacobian Jac; Integration Int; }
                //                If (CAVITY == 0)
                Integral { [1 / r[] * nu_r[] * Dof{d e_phi_star}, {d e_phi_star} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                // Boundary
                // Include Sommerfeld boundary terms only for a non-cavity problem. 
                // For a cavity problem (homogeneous Neumann), the boundary terms can simply be omitted. 
                // Because e_phi_star = r * e_phi, the second integral needs to contain 1/r instead of r
                If (CAVITY == 0)
                Integral { [ I[] * r[] * nu_r[] * k_inf * Dof{e_inplane}, {e_inplane}];
                    In Bnd_Truncated; Jacobian Jac; Integration Int;}
                Integral { [ I[] * nu_r[] * k_inf / r[] * Dof{e_phi_star}, {e_phi_star}];
                    In Bnd_Truncated; Jacobian Jac; Integration Int;}
                EndIf
            }
        ElseIf (CAVITY == 1)
            Equation {
                Integral { DtDtDof[1 / c0^2 * eps_r[] * r[] * Dof{e_inplane} , {e_inplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { DtDtDof[1 / c0^2 * eps_r[] / r[] * Dof{e_phi_star} , {e_phi_star} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [nu_r[] * r[] * Dof{d e_inplane} , {d e_inplane} ]; 
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [m^2 / r[] * nu_r[] * Dof{e_inplane} , {e_inplane} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
                // 
                Integral { [M_SIGN * m / r[] * nu_r[] * Dof{d e_phi_star}, {e_inplane} ];  
                    In Dielectric; Jacobian Jac; Integration Int; } 
                Integral { [M_SIGN * m / r[] * nu_r[] * Dof{e_inplane}, {d e_phi_star} ];  
                    In Dielectric; Jacobian Jac; Integration Int; }
                //
                Integral { [1 / r[] * nu_r[] * Dof{d e_phi_star}, {d e_phi_star} ];
                    In Dielectric; Jacobian Jac; Integration Int; }
            }
        EndIf
        
        // projection to recover e_phi
        Equation {
			// weak formulation of e_phi = e_phi_star / r
            Integral { [Dof{e_outofplane}, {e_outofplane}];
                In Dielectric; Jacobian Jac; Integration Int; }
            Integral { [- 1 / r[] * Dof{e_phi_star}, {e_outofplane}];
                In Dielectric; Jacobian Jac; Integration Int; }
        }
    }
}

Resolution {
  If (CAVITY == 0)
    { Name solution_waveguide;
        System {
        { Name A ; NameOfFormulation wave_formulation~{ANSATZ} ; Type ComplexValue ; }
        }
        Operation {
        Generate[A] ; Solve[A] ; SaveSolution[A] ;
        }
    }
    { Name excitation_inplane; // Resolution for inplane excitation
        System {
            {Name B; NameOfFormulation excitation_dirichlet_inplane; Type ComplexValue; DestinationSystem A; }
        }
        Operation {
        Generate[B]; Solve[B]; TransferSolution[B]; //solve this first, then transfer solution to system A
        }
    }
    
    { Name excitation_outofplane; // Resolution for outofplane excitation
        System {
            {Name C; NameOfFormulation excitation_dirichlet_outofplane~{ANSATZ}; Type ComplexValue; DestinationSystem A; }
        }
        Operation {
        Generate[C]; Solve[C]; TransferSolution[C]; //solve this first, then transfer solution to system A
        }
    }
  ElseIf (CAVITY == 1)
    { Name solution_cavity;
        System {
            { Name D ; NameOfFormulation wave_formulation~{ANSATZ} ; Type ComplexValue;}
        }
        Operation {
            GenerateSeparate[D];
            EigenSolve [D, no_of_eigenvalues, target_freqs^2, 0];
            SaveSolutions[D];
        }
    }
  EndIf
}

PostProcessing {
  { Name e_field_post_process ; NameOfFormulation wave_formulation~{ANSATZ};
    Quantity {
        { Name eRZ ; Value { Local{ [ {e_inplane}] ; In Dielectric ; Jacobian Jac ; } } }
        { Name ePhi ; Value { Local{ [{e_outofplane} ] ; In Dielectric ; Jacobian Jac ; } } }
        { Name e ; Value {Local { [{e_inplane} + unitPhi[] *  {e_outofplane}] ; In Dielectric ; Jacobian Jac; }}}
        { Name e_port_in; Value {Local { [{e_inplane}]; In Port_1 ; Jacobian Jac ; }}} // inplane component of e on Port_1. For debug purposes only. 
        { Name e_port_out; Value {Local { [{e_outofplane}]; In Port_1 ; Jacobian Jac ; }}} // outofplane component of e on Port_1. For debug purposes only. 
        // norm of error between numerical and analytical solution integrated over the domain
        { Name squared_err_inplane; Value{
            Integral{ Type Global;
                    [SquNorm[f_ana_inplane[] - {e_inplane}]]; 
                    In Dielectric; Jacobian Jac; Integration Int;}}
        }
        { Name squared_norm_inplane; Value{
            Integral{ Type Global; 
                    [SquNorm[f_ana_inplane[]]];
                    In Dielectric; Jacobian Jac; Integration Int;}}
        }
        // If the e_phi_star ANSATZ is used, compare e_phi_star with r[] * e_phi_ana. This avoids the additional error introduced by the projection used to recover e_phi
        If (ANSATZ == 2)
        { Name squared_norm_outofplane; Value{
            Integral{ Type Global;
                    [SquNorm[r[] * f_ana_outofplane[]]];
                    In Dielectric; Jacobian Jac; Integration Int;}}
        }
        { Name squared_err_outofplane; Value{
            Integral{ Type Global;
                    [SquNorm[r[] * f_ana_outofplane[] - {e_phi_star}]];
                    In Dielectric; Jacobian Jac; Integration Int;}}
        }
        ElseIf (ANSATZ == 1)
        { Name squared_norm_outofplane; Value{
            Integral{ Type Global;
                    [SquNorm[f_ana_outofplane[]]];
                    In Dielectric; Jacobian Jac; Integration Int;}}
        }
        { Name squared_err_outofplane; Value{
            Integral{ Type Global;
                    [SquNorm[f_ana_outofplane[] - {e_outofplane}]];
                    In Dielectric; Jacobian Jac; Integration Int;}}
        }
        EndIf
        { Name EigenValuesReal; Value { Local{ [$EigenvalueReal] ; In Print_Point; Jacobian Jac; } } } 
    }
  }
}

PostOperation {
    { Name e_field_post_op ; NameOfPostProcessing e_field_post_process;
    Operation {
      Print[ eRZ, OnElementsOf Dielectric, File "waveguide_erz.pos", EigenvalueLegend ] ;
      Print[ ePhi, OnElementsOf Dielectric, File "waveguide_ephi.pos", EigenvalueLegend ] ;
      Print[ e, OnElementsOf Dielectric, File "waveguide_e.pos", EigenvalueLegend ];
      //
      // Convergence study
      //
      // print general info
      If (output_convergence_logs == 1)
        Echo[StrCat[mode_type_str ,Sprintf["_%g%g mode (", m, n], mode_sign_str, ") in a ", cavity_str, Sprintf[" domain, size R=%f, L=%f at resolution %f. At excitation frequency omega=%f, that's %g elements per wavelength. ", R, L, res, omega, no_of_ele_per_lambda], ANSATZ_str, " was chosen. ", order_str, " basis functions used."], Format ElementTable, File > convergence_output_filename];    
        // print integral over error norm
        Echo["Integral of squared error, inplane:", Format Table, File > convergence_output_filename];
        Print[squared_err_inplane, OnGlobal, File > convergence_output_filename, Format Table];
        Echo["Integral of squared norm of analytical solution, inplane:", Format Table, File > convergence_output_filename];
        Print[squared_norm_inplane, OnGlobal, File > convergence_output_filename, Format Table];
        Echo["Integral of squared error, outofplane:", Format Table, File > convergence_output_filename];
        Print[squared_err_outofplane, OnGlobal, File > convergence_output_filename, Format Table];
        Echo["Integral of squared norm of analytical solution, outofplane:", Format Table, File > convergence_output_filename];
        Print[squared_norm_outofplane, OnGlobal, File > convergence_output_filename, Format Table];
        Echo["", Format Table, File > convergence_output_filename];
        //
        If (CAVITY) 
            Print [EigenValuesReal, OnElementsOf Print_Point, Format TimeTable, File > eigenvalues_output_filename]; 
        EndIf
    EndIf
    }
  }
}
