#!/bin/bash

###
###	user defined arguments
###

total_length=0.2		#length of the waveguide
radius=0.035			#radius of the waveguide
radial_freq=1e+11		#radial frequency at which the field is excited
ele_per_lambda_start=5		#number of elements per wavelength to start at.
ele_per_lambda_steps=6		#number of convergence data points (number of refinement steps + 1)
complete_basis=1		#when true, BF_Edge_2E etc are included.

###	files

log_file_name="convergence_study.log"	#PostOperation writes results here
raw_data_file="convergence_study_raw"	#awk scrapes log_file_name and writes results here
problem_base_name="waveguide"	#base name of .pro, .geo, etc.

###	Operations to perform
op_solve=0	# call getdp to solve, generate logs
op_scrape=1	# scrape existing logs to recover raw numbers
op_process=1	# process raw numbers with octave



directory=$(dirname "$(readlink -f "$0")")	#directory of this script
if [[ complete_basis -eq 1 ]]
then
	order_dir=high_order
else
	order_dir=low_order
fi

# decreasing refinement steps by one to avoid arithmetic later on
let ele_per_lambda_steps=$ele_per_lambda_steps-1

if [[ $op_solve -eq 1 ]]
then

rm -f $directory/convergence_waveguide/$order_dir/$log_file_name # remove old logs
echo "Starting convergence study of Q3D at "$(date) >> $directory/convergence_waveguide/$order_dir/$log_file_name

for mode_type in 1 2 # 1 for TE, 2 for TM
do
	for m in 0 1 2
	do
		for n in 1 2 3
		do
				#	echo $number_of_subdomains && sleep 10
				for ele_per_lambda in $(octave-cli --eval "disp($ele_per_lambda_start.*2.^[0:$ele_per_lambda_steps]);")
				do	
					#ansatz
					if (($m > 0))
					then
						ansatz=2	# e-phi-star
					else
						ansatz=1	# no ansatz
					fi

					#mode sign
					if [[ $mode_type -eq 1 ]]
					then
						mode_sign=-1
					else
						mode_sign=1
					fi

					# argument strings
					gmsh_args="-setnumber L $total_length -setnumber R $radius -setnumber no_of_ele_per_lambda $ele_per_lambda" # -setnumber omega $radial_freq"
					
					getdp_args=$gmsh_args" -setnumber COMPLETE_BASIS $complete_basis -setnumber EXC_TYPE $mode_type -setnumber M_SIGN $mode_sign -setnumber m $m -setnumber n $n -setnumber ANSATZ $ansatz -setnumber output_convergence_logs 1 -setstring convergence_output_filename $directory/convergence_waveguide/$order_dir/$log_file_name"
					# remove old mesh, database and output files
					rm -f $directory/$problem_base_name.msh $directory/$problem_base_name.db $directory/out/*

					# generate mesh
					gmsh $directory/$problem_base_name.geo $gmsh_args -2 # mesh 2D, save and exit

					# solve
					getdp $directory/$problem_base_name.pro $getdp_args -solve solution_waveguide -pos e_field_post_op
				done
			
		done
	done
done

echo -e "Convergence study of Q3D ended at "$(date)"\n\n=================================================================================================\n" >> $directory/convergence_waveguide/$order_dir/$log_file_name
fi

# scrape log for raw numbers
if [[ $op_scrape -eq 1 ]]
then

rm -f $directory/convergence_waveguide/$order_dir/$raw_data_file
awk '/^0 * */ {print $0}' $directory/convergence_waveguide/$order_dir/$log_file_name >> $directory/convergence_waveguide/$order_dir/$raw_data_file

fi

# process raw numbers in octave
if [[ $op_process -eq 1 ]]
then

	octave --path $directory --eval "compute_rel_err_waveguide(2, 3, $ele_per_lambda_start, $ele_per_lambda_steps, 1, '$directory/convergence_waveguide/$order_dir/$raw_data_file', '$directory/convergence_waveguide/$order_dir/convergence_waveguide_results', '$directory/../../../figures/convergence/non_ddm/Q3D/$order_dir');"

fi
