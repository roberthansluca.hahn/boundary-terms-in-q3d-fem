function compute_rel_err_waveguide(m_max=2, n_max=3, start_res=5, res_steps=4, print_figures=false, ...
  source_file='convergence_waveguide/low_order/convergence_study_raw', target_file='convergence_waveguide/low_order/convergence_waveguide_results', figure_dest='../../../figures/convergence/non_ddm/Q3D/low_order')
data=load(source_file)(:, 2); # load source file. Only take 2nd column (real part of L2-Norm)
idx=1; # starting index
idx_step=4; # 4 lines for each mode (inplane and outofplane, err. and analytical solution)
rel_errs = zeros(2, m_max+1, n_max, 3, res_steps+1); # save rel erros here. 
# Index scheme is {1 for TE, 2 for TM}, {m+1}, {n}, {1 for inplane, 2 for outofplane, 3 for combined}, {refinement step}
for type=1:2
    for m=0:m_max
        for n=1:n_max
                # extract squared err norm, squared analytical solution for in- and outofplane 
                squ_err_inplane = data([idx:idx_step:idx+idx_step*res_steps]);
                squ_norm_inplane = data([idx+1:idx_step:idx+idx_step*res_steps+1]);
                squ_err_outofplane = data([idx+2:idx_step:idx+idx_step*res_steps+2]);
                squ_norm_outofplane = data([idx+3:idx_step:idx+idx_step*res_steps+3]);
                rel_errs(type, m+1, n, 1, :) = sqrt(squ_err_inplane ./ squ_norm_inplane);
                rel_errs(type, m+1, n, 2, :) = sqrt(squ_err_outofplane ./ squ_norm_outofplane);
                rel_errs(type, m+1, n, 3, :) = sqrt((squ_err_outofplane+squ_err_inplane) ./ (squ_norm_outofplane+squ_norm_inplane));
                idx+=idx_step*(res_steps+1); # step to beginning of next mode
        end
    end
end

xvalues = start_res .* 2.^[0:res_steps];
### slope
convergence_slope = @(xval, yval) ...
(log2(yval(2:end)) - log2(yval(1:end-1))) ./ (log2(xval(2:end)) - log2(xval(1:end-1)));

# initialize slope result vectors. Element i is the convergence slope between data point i and i+1.
te_slopes_inplane = zeros(m_max+1, n_max, res_steps);
te_slopes_outofplane = zeros(m_max+1, n_max, res_steps);
tm_slopes_inplane = zeros(m_max+1, n_max, res_steps);
tm_slopes_outofplane = zeros(m_max+1, n_max, res_steps);
for m=1:m_max+1
  for n=1:n_max
    # calculate convergence slopes. squeeze is needed to obtain the correct dimensionality.
    te_slopes_inplane(m, n, :) = convergence_slope(xvalues, squeeze(abs(rel_errs(1, m, n, 1, :)))');
    te_slopes_outofplane(m, n, :) = convergence_slope(xvalues, squeeze(abs(rel_errs(1, m, n, 2, :)))');
    tm_slopes_inplane(m, n, :) = convergence_slope(xvalues, squeeze(abs(rel_errs(2, m, n, 1, :)))');
    tm_slopes_outofplane(m, n, :) = convergence_slope(xvalues, squeeze(abs(rel_errs(2, m, n, 2, :)))');
  end
end

### save slope results
res_file = fopen(target_file, 'w');
fputs(res_file, "TE, inplane\n");
for m=1:m_max+1
  for n=1:n_max
    dlmwrite(res_file, squeeze(te_slopes_inplane(m, n, :))');
  end
end
fputs(res_file, "TE, outofplane\n");
for m=1:m_max+1
  for n=1:n_max
    dlmwrite(res_file, squeeze(te_slopes_outofplane(m, n, :))');
  end
end
#
fputs(res_file, "TM, inplane\n");
for m=1:m_max+1
  for n=1:n_max
    dlmwrite(res_file, squeeze(tm_slopes_inplane(m, n, :))');
  end
end
fputs(res_file, "TM, outofplane\n");
for m=1:m_max+1
  for n=1:n_max
    dlmwrite(res_file, squeeze(tm_slopes_outofplane(m, n, :))');
  end
end
fclose(res_file);
#
### save results in csv files
# 

for mode_type =1:2
    for plane=1:3
      tmp_mat = zeros(res_steps+1, m_max+1 + n_max);
      tmp_str = "elements";
      for m=0:m_max
        for n=1:n_max
          tmp_mat(:, m*n_max+n) = rel_errs(mode_type, m+1, n, plane, :);
          tmp_str = strcat(tmp_str, sprintf(",m=%g-n=%g",m,n));
        end
      end
      tmp_file_str = strcat(figure_dest, "/raw/waveguide");
      if (mode_type == 1) tmp_file_str = strcat(tmp_file_str, "_te"); else tmp_file_str = strcat(tmp_file_str, "_tm"); end
      if (plane == 1) tmp_file_str = strcat(tmp_file_str, "_inplane"); elseif (plane==2) tmp_file_str = strcat(tmp_file_str, "_outofplane"); else tmp_file_str = strcat(tmp_file_str, "_combined"); end
      tmp_file_str = strcat(tmp_file_str, ".csv");
      tmp_file = fopen(tmp_file_str, 'w');
      fputs(tmp_file, strcat(tmp_str, "\n"));

      csvwrite( tmp_file, ...
        [xvalues', tmp_mat]);
      fclose(tmp_file);
    end
end
clear tmp_file_str; 
clear tmp_file;
clear tmp_mat;
clear tmp_str;
end
