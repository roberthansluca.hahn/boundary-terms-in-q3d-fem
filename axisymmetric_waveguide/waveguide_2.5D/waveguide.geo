/*
This file describes the geometry of an axisymmetric (cylindrical) waveguide.
*/

Include "../shared_pro.dat";

// points
Point(1) = {0,0,0, res};
Point(2) = {R, 0, 0, res};
Point(3) = {R, L, 0, res};
Point(4) = {0, L, 0, res};

Point (10000) = {0,0,0,0};

// lines
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3, 4};
Line(4) = {4, 1};

// surface
Curve Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};

Physical Line (1000) = {1}; //z=0
Physical Line (1001) = {2}; //r=R
Physical Line (1002) = {3}; //z=L
Physical Line (1003) = {4}; //symmetry axis 
Physical Surface (1004) = {6};

Physical Point  (10000) = {10000};

Transfinite Surface "*";
